#!/bin/bash
export FLASK_APP=/timeslime-server/timeslime_server
export FLASK_ENV=development
pipenv run flask create-user --username $USER --password $PASSWORD
pipenv run flask create-user --username $TEST_USER --password $TEST_PASSWORD
pipenv run waitress-serve --port 8000 --expose-tracebacks --call 'timeslime_server:create_app'