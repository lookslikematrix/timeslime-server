# timeslime-server

It's a server for your timeslime data.

## Develop

~~~bash
git clone https://gitlab.com/lookslikematrix/timeslime-server
cd timeslime-server
pip install pipenv
pipenv install -d
export FLASK_ENV=development
export FLASK_APP=timeslime_server
pipenv run flask run
~~~

## Use docker container

Change user settings for your own security.

For testing:
~~~bash
docker run --name timeslime-server -p 8000:8000 -e USER=test_user -e PASSWORD=supersecret -d lookslikematrix/timeslime-server:latest
~~~

For production with persistent data:
~~~bash
mkdir -p /var/lib/timeslime
docker run --name timeslime-server -p 8000:8000 -e USER=test_user -e PASSWORD=supersecret -d -v /var/lib/timeslime/data:/timeslime-server/data lookslikematrix/timeslime-server:latest
~~~