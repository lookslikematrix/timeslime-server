"""test settings routes"""
import os
import tempfile

import pytest

from timeslime_server import create_app


@pytest.fixture(name="client")
def fixture_client():
    """create test client"""
    db_fd, db_path = tempfile.mkstemp()
    app = create_app({"TESTING": True, "DATABASE": db_path})

    with app.test_client() as client:
        yield client

    os.close(db_fd)
    os.unlink(db_path)


def test_get_me_no_user_error(client):
    """test GET me"""
    # arrange & act
    response = client.get("/api/v1/me")

    # assert
    assert response.status_code == 403
    assert response.get_data() == b"Please provide user credentials with your request"


def test_get_me_wrong_user_error(client):
    """test GET me"""
    # arrange & act
    response = client.get("/api/v1/me", auth=("wrong_user", "password"))

    # assert
    assert response.status_code == 403
    assert response.get_data() == b"User is not authenticated"


def test_get_me_success(client):
    """test GET me"""
    # arrange
    headers = {
        "Content-Type": "application/json",
    }

    # act
    response = client.get(
        "/api/v1/me", headers=headers, auth=("test_user", "test_password")
    )

    # assert
    assert response.status_code == 200
    response_json = response.get_json()
    assert response_json["username"] == "test_user"
