"""test timespans routes"""
import os
import tempfile
from datetime import datetime
from json import dumps

import pytest

from timeslime_server import create_app


@pytest.fixture(name="client")
def fixture_client():
    """create test client"""
    db_fd, db_path = tempfile.mkstemp()
    app = create_app({"TESTING": True, "DATABASE": db_path})

    with app.test_client() as client:
        yield client

    os.close(db_fd)
    os.unlink(db_path)


def test_post_timespan_no_user_error(client):
    """test POST timespan"""
    # arrange & act
    response = client.post("/api/v1/timespans")

    # assert
    assert response.status_code == 403
    assert response.get_data() == b"Please provide user credentials with your request"


def test_post_timespan_wrong_user_error(client):
    """test POST timespan"""
    # arrange & act
    response = client.post("/api/v1/timespans", auth=("wrong_user", "password"))

    # assert
    assert response.status_code == 403
    assert response.get_data() == b"User is not authenticated"


def test_post_timespan_no_data_error(client):
    """test POST timespan"""
    # arrange & act
    response = client.post("/api/v1/timespans", auth=("test_user", "test_password"))

    # assert
    assert response.status_code == 400
    assert response.get_data() == b"Content-Type must be application/json"


def test_post_timespan_no_json_error(client):
    """test POST timespan"""
    # arrange
    headers = {"Content-Type": "application/txt"}
    data = "test"

    # act
    response = client.post(
        "/api/v1/timespans",
        headers=headers,
        data=data,
        auth=("test_user", "test_password"),
    )

    # assert
    assert response.status_code == 400
    assert response.get_data() == b"Content-Type must be application/json"


def test_post_timespan_wrong_json_error(client):
    """test POST timespan"""
    # arrange
    headers = {"Content-Type": "application/json"}
    data = "test"

    # act
    response = client.post(
        "/api/v1/timespans",
        headers=headers,
        data=data,
        auth=("test_user", "test_password"),
    )

    # assert
    assert response.status_code == 400


def test_post_timespan_correct_json_wrong_object_error(client):
    """test POST timespan"""
    # arrange
    headers = {"Content-Type": "application/json"}
    data = dumps({"foo": 2})

    # act
    response = client.post(
        "/api/v1/timespans",
        headers=headers,
        data=data,
        auth=("test_user", "test_password"),
    )

    # assert
    assert response.status_code == 400
    assert response.get_data() == b"Could not serialize object"

def test_post_timespan_correct_json_correct_object_success(client):
    """test POST timespan"""
    # arrange
    headers = {"Content-Type": "application/json"}
    data = {
        "id": "51288003-49dc-4e34-82d4-51f84fd9e59d",
        "start_time": "2021-08-08 21:47:20.833440+00:00",
        "stop_time": "2021-08-08 21:47:26.257399+00:00",
        "created_at": None,
        "updated_at": "2021-08-08 21:47:26.257399+00:00",
    }

    # act
    response = client.post(
        "/api/v1/timespans",
        headers=headers,
        json=data,
        auth=("test_user", "test_password"),
    )

    # assert
    assert response.status_code == 200
    response_data = response.get_json()
    assert response_data["id"] == data["id"]
    assert response_data["start_time"] == data["start_time"]
    assert response_data["stop_time"] == data["stop_time"]
    assert response_data["created_at"] is not None
    assert response_data["updated_at"] is not None

def test_post_timespan_array_with_one_wrong_object_only_right_is_saved_success(client):
    """test POST timespan"""
    # arrange
    headers = {"Content-Type": "application/json"}
    data1 = {
        "id": "51288003-49dc-4e34-82d4-51f84fd9e59d",
        "start_time": "2021-08-08 21:47:20.833440+00:00",
        "stop_time": "2021-08-08 21:47:26.257399+00:00",
    }
    data2 = {
        "id": "51288003-49dc-4e34-82d4-51f84fd9e59b",
        "stop_time": "2021-08-08 21:47:26.257399+00:00",
    }

    # act
    response = client.post(
        "/api/v1/timespans",
        headers=headers,
        json=[data1, data2],
        auth=("test_user", "test_password"),
    )

    # assert
    assert response.status_code == 200
    response_data = response.get_json()
    assert len(response_data) == 1
    assert response_data[0]["id"] == data1["id"]
    assert response_data[0]["start_time"] == data1["start_time"]
    assert response_data[0]["stop_time"] == data1["stop_time"]
    assert response_data[0]["created_at"] is not None
    assert response_data[0]["updated_at"] is not None


def test_post_timespan_without_created(client):
    """test POST timespan"""
    # arrange
    headers = {"Content-Type": "application/json"}
    data = {
        "id": "51288003-49dc-4e34-82d4-51f84fd9e59d",
        "start_time": "2021-08-08 21:47:20.833440+00:00",
        "stop_time": "2021-08-08 21:47:26.257399+00:00",
    }

    # act
    response = client.post(
        "/api/v1/timespans",
        headers=headers,
        json=[data],
        auth=("test_user", "test_password"),
    )

    # assert
    assert response.status_code == 200
    response_data = response.get_json()
    assert response_data[0]["id"] == data["id"]
    assert response_data[0]["start_time"] == data["start_time"]
    assert response_data[0]["stop_time"] == data["stop_time"]
    assert response_data[0]["created_at"] is not None
    assert response_data[0]["updated_at"] is not None

def test_get_timespan_no_user_error(client):
    """test GET timespan"""
    # arrange & act
    response = client.get("/api/v1/timespans")

    # assert
    assert response.status_code == 403
    assert response.get_data() == b"Please provide user credentials with your request"


def test_get_timespan_wrong_user_error(client):
    """test GET timespan"""
    # arrange & act
    response = client.get("/api/v1/timespans", auth=("wrong_user", "password"))

    # assert
    assert response.status_code == 403
    assert response.get_data() == b"User is not authenticated"

def test_get_timespans_no_timespans_success(client):
    """test GET timespan"""
    # arrange
    headers = {"Content-Type": "application/json"}
    # act
    response = client.get(
        "/api/v1/timespans", headers=headers, auth=("test_user", "test_password")
    )

    # assert
    assert response.status_code == 200
    response_json = response.get_json()
    assert response_json["data"] == []
    assert response_json["request_time"] is not None
    request_time = datetime.strptime(
        response_json["request_time"], "%Y-%m-%d %H:%M:%S.%f"
    )
    assert isinstance(request_time, datetime) is True

def test_get_timespans_timespans_success(client):
    """test GET timespan"""
    # arrange
    headers = {"Content-Type": "application/json"}
    data = {
        "id": "51288003-49dc-4e34-82d4-51f84fd9e59d",
        "start_time": "2021-08-08 21:47:20.833440+00:00",
        "stop_time": "2021-08-08 21:47:26.257399+00:00",
    }
    response = client.post(
        "/api/v1/timespans",
        headers=headers,
        json=data,
        auth=("test_user", "test_password"),
    )

    # act
    response = client.get(
        "/api/v1/timespans", headers=headers, auth=("test_user", "test_password")
    )

    # assert
    assert response.status_code == 200
    response_json = response.get_json()
    assert response_json["request_time"] is not None
    request_time = datetime.strptime(
        response_json["request_time"], "%Y-%m-%d %H:%M:%S.%f"
    )
    assert isinstance(request_time, datetime) is True
    response_data = response_json["data"]
    assert len(response_data) == 1
    assert response_data[0]["id"] == data["id"]
    assert response_data[0]["start_time"] == data["start_time"]
    assert response_data[0]["stop_time"] == data["stop_time"]
    assert response_data[0]["created_at"] is not None
    assert response_data[0]["updated_at"] is not None


def test_get_timespans_with_wrong_filter_date_timespans_success(client):
    """test GET timespan"""
    # arrange
    headers = {"Content-Type": "application/json"}
    data = [
        {
            "id": "51288003-49dc-4e34-82d4-51f84fd9e59d",
            "start_time": "2021-08-08 21:47:20.833440",
            "stop_time": "2021-08-08 21:47:26.257399",
            "created_at": "2021-10-18 17:00:00.000000",
            "updated_at": "2021-10-18 17:00:00.000000",
        },
        {
            "id": "51288003-49dc-4e34-82d4-51f84fd9e59b",
            "start_time": "2021-08-08 21:47:20.833440",
            "stop_time": "2021-08-08 21:47:26.257399",
            "created_at": "2021-10-18 17:00:00.000000",
            "updated_at": "2021-10-18 17:30:00.000000",
        },
    ]
    response = client.post(
        "/api/v1/timespans",
        headers=headers,
        json=data,
        auth=("test_user", "test_password"),
    )

    # act
    response = client.get(
        "/api/v1/timespans",
        headers=headers,
        json={"filter_datetime": "no_time"},
        auth=("test_user", "test_password"),
    )

    # assert
    assert response.status_code == 200
    response_json = response.get_json()
    assert response_json["request_time"] is not None
    request_time = datetime.strptime(
        response_json["request_time"], "%Y-%m-%d %H:%M:%S.%f"
    )
    assert isinstance(request_time, datetime) is True
    response_data = response_json["data"]
    assert len(response_data) == 2


def test_get_timespans_with_wrong_from_parameter_timespans_success(client):
    """test GET timespan"""
    # arrange
    headers = {"Content-Type": "application/json"}
    data = [
        {
            "id": "51288003-49dc-4e34-82d4-51f84fd9e59d",
            "start_time": "2021-08-08 21:47:20.833440",
            "stop_time": "2021-08-08 21:47:26.257399",
            "created_at": "2021-10-18 17:00:00.000000",
            "updated_at": "2021-10-18 17:00:00.000000",
        },
        {
            "id": "51288003-49dc-4e34-82d4-51f84fd9e59b",
            "start_time": "2021-08-08 21:47:20.833440",
            "stop_time": "2021-08-08 21:47:26.257399",
            "created_at": "2021-10-18 17:00:00.000000",
            "updated_at": "2021-10-18 17:30:00.000000",
        },
    ]
    response = client.post(
        "/api/v1/timespans",
        headers=headers,
        json=data,
        auth=("test_user", "test_password"),
    )

    # act
    response = client.get(
        "/api/v1/timespans?from=nodate",
        headers=headers,
        auth=("test_user", "test_password"),
    )

    # assert
    assert response.status_code == 200
    response_json = response.get_json()
    assert response_json["request_time"] is not None
    request_time = datetime.strptime(
        response_json["request_time"], "%Y-%m-%d %H:%M:%S.%f"
    )
    assert isinstance(request_time, datetime) is True
    response_data = response_json["data"]
    assert len(response_data) == 2


def test_get_timespans_with_wrong_to_parameter_timespans_success(client):
    """test GET timespan"""
    # arrange
    headers = {"Content-Type": "application/json"}
    data = [
        {
            "id": "51288003-49dc-4e34-82d4-51f84fd9e59d",
            "start_time": "2021-08-08 21:47:20.833440",
            "stop_time": "2021-08-08 21:47:26.257399",
            "created_at": "2021-10-18 17:00:00.000000",
            "updated_at": "2021-10-18 17:00:00.000000",
        },
        {
            "id": "51288003-49dc-4e34-82d4-51f84fd9e59b",
            "start_time": "2021-08-08 21:47:20.833440",
            "stop_time": "2021-08-08 21:47:26.257399",
            "created_at": "2021-10-18 17:00:00.000000",
            "updated_at": "2021-10-18 17:30:00.000000",
        },
    ]
    response = client.post(
        "/api/v1/timespans",
        headers=headers,
        json=data,
        auth=("test_user", "test_password"),
    )

    # act
    response = client.get(
        "/api/v1/timespans?to=nodate",
        headers=headers,
        auth=("test_user", "test_password"),
    )

    # assert
    assert response.status_code == 200
    response_json = response.get_json()
    assert response_json["request_time"] is not None
    request_time = datetime.strptime(
        response_json["request_time"], "%Y-%m-%d %H:%M:%S.%f"
    )
    assert isinstance(request_time, datetime) is True
    response_data = response_json["data"]
    assert len(response_data) == 2


def test_get_timespans_with_filter_date_timespans_success(client):
    """test GET timespan"""
    # arrange
    headers = {"Content-Type": "application/json"}
    data = [
        {
            "id": "51288003-49dc-4e34-82d4-51f84fd9e59d",
            "start_time": "2021-08-08 21:47:20.833440+00:00",
            "stop_time": "2021-08-08 21:47:26.257399+00:00",
        },
        {
            "id": "51288003-49dc-4e34-82d4-51f84fd9e59b",
            "start_time": "2021-08-08 21:47:20.833440+00:00",
            "stop_time": "2021-08-08 21:47:26.257399+00:00",
        },
        {
            "id": "51288003-49dc-4e34-82d4-51f84fd9e59c",
            "start_time": "2021-08-08 21:47:20.833440+00:00",
            "stop_time": "2021-08-08 21:47:26.257399+00:00",
        },
    ]
    response = client.post(
        "/api/v1/timespans",
        headers=headers,
        json=data,
        auth=("test_user", "test_password"),
    )

    # act
    response = client.get(
        "/api/v1/timespans",
        headers=headers,
        json={"filter_datetime": response.json[0]["updated_at"]},
        auth=("test_user", "test_password"),
    )

    # assert
    assert response.status_code == 200
    response_json = response.get_json()
    assert response_json["request_time"] is not None
    request_time = datetime.strptime(
        response_json["request_time"], "%Y-%m-%d %H:%M:%S.%f"
    )
    assert isinstance(request_time, datetime) is True
    response_data = response_json["data"]
    assert len(response_data) == 2
    assert response_data[0]["id"] == data[1]["id"]
    assert response_data[0]["start_time"] == data[1]["start_time"]
    assert response_data[0]["stop_time"] == data[1]["stop_time"]
    assert response_data[0]["created_at"] is not None
    assert response_data[0]["updated_at"] is not None
    assert response_data[1]["id"] == data[2]["id"]
    assert response_data[1]["start_time"] == data[2]["start_time"]
    assert response_data[1]["stop_time"] == data[2]["stop_time"]
    assert response_data[1]["created_at"] is not None
    assert response_data[1]["updated_at"] is not None


def test_get_timespans_with_from_as_param_timespans_success(client):
    """test GET timespan"""
    # arrange
    headers = {"Content-Type": "application/json"}
    data = [
        {
            "id": "51288003-49dc-4e34-82d4-51f84fd9e59d",
            "start_time": "2021-08-08 21:47:20.833440+00:00",
            "stop_time": "2021-08-08 21:47:26.257399+00:00",
        },
        {
            "id": "51288003-49dc-4e34-82d4-51f84fd9e59b",
            "start_time": "2021-08-08 21:47:21.833440+00:00",
            "stop_time": "2021-08-08 21:47:26.257399+00:00",
        },
        {
            "id": "51288003-49dc-4e34-82d4-51f84fd9e59c",
            "start_time": "2021-08-08 21:47:28.833440+00:00",
            "stop_time": "2021-08-08 21:47:30.257399+00:00",
        },
    ]
    response = client.post(
        "/api/v1/timespans",
        headers=headers,
        json=data,
        auth=("test_user", "test_password"),
    )

    # act
    response = client.get(
        "/api/v1/timespans",
        headers=headers,
        query_string={"from": response.json[0]["start_time"]},
        auth=("test_user", "test_password"),
    )

    # assert
    assert response.status_code == 200
    response_json = response.get_json()
    assert response_json["request_time"] is not None
    request_time = datetime.strptime(
        response_json["request_time"], "%Y-%m-%d %H:%M:%S.%f"
    )
    assert isinstance(request_time, datetime) is True
    response_data = response_json["data"]
    assert len(response_data) == 2
    assert response_data[0]["id"] == data[1]["id"]
    assert response_data[0]["start_time"] == data[1]["start_time"]
    assert response_data[0]["stop_time"] == data[1]["stop_time"]
    assert response_data[0]["created_at"] is not None
    assert response_data[0]["updated_at"] is not None
    assert response_data[1]["id"] == data[2]["id"]
    assert response_data[1]["start_time"] == data[2]["start_time"]
    assert response_data[1]["stop_time"] == data[2]["stop_time"]
    assert response_data[1]["created_at"] is not None
    assert response_data[1]["updated_at"] is not None


def test_get_timespans_with_from_and_updated_at_as_param_timespans_success(client):
    """test GET timespan"""
    # arrange
    headers = {"Content-Type": "application/json"}
    data = [
        {
            "id": "51288003-49dc-4e34-82d4-51f84fd9e59d",
            "start_time": "2021-08-08 21:47:20.833440+00:00",
            "stop_time": "2021-08-08 21:47:26.257399+00:00",
            "updated_at": "2021-08-08 21:47:25.257399+00:00",
        },
        {
            "id": "51288003-49dc-4e34-82d4-51f84fd9e59b",
            "start_time": "2021-08-08 21:47:20.833440+00:00",
            "stop_time": "2021-08-08 21:47:26.257399+00:00",
            "updated_at": "2021-08-08 21:47:26.257399+00:00",
        },
        {
            "id": "51288003-49dc-4e34-82d4-51f84fd9e59c",
            "start_time": "2021-08-08 21:47:20.833440+00:00",
            "stop_time": "2021-08-08 21:47:26.257399+00:00",
            "updated_at": "2021-08-08 21:47:28.257399+00:00",
        },
    ]
    response = client.post(
        "/api/v1/timespans",
        headers=headers,
        json=data,
        auth=("test_user", "test_password"),
    )

    # act
    response = client.get(
        "/api/v1/timespans",
        headers=headers,
        query_string={
            "from": "2021-08-08",
            "updated_at": response.json[1]["updated_at"],
        },
        auth=("test_user", "test_password"),
    )

    # assert
    assert response.status_code == 200
    response_json = response.get_json()
    assert response_json["request_time"] is not None
    request_time = datetime.strptime(
        response_json["request_time"], "%Y-%m-%d %H:%M:%S.%f"
    )
    assert isinstance(request_time, datetime) is True
    response_data = response_json["data"]
    assert len(response_data) == 1
    assert response_data[0]["id"] == data[2]["id"]
    assert response_data[0]["start_time"] == data[2]["start_time"]
    assert response_data[0]["stop_time"] == data[2]["stop_time"]
    assert response_data[0]["created_at"] is not None
    assert response_data[0]["updated_at"] is not None


def test_get_timespans_with_to_as_param_timespans_success(client):
    """test GET timespan"""
    # arrange
    headers = {"Content-Type": "application/json"}
    data = [
        {
            "id": "51288003-49dc-4e34-82d4-51f84fd9e59d",
            "start_time": "2021-08-08 21:47:20.833440+00:00",
            "stop_time": "2021-08-08 21:47:26.257399+00:00",
        },
        {
            "id": "51288003-49dc-4e34-82d4-51f84fd9e59b",
            "start_time": "2021-08-08 21:47:21.833440+00:00",
            "stop_time": "2021-08-08 21:47:27.257399+00:00",
        },
        {
            "id": "51288003-49dc-4e34-82d4-51f84fd9e59c",
            "start_time": "2021-08-08 21:47:28.833440+00:00",
            "stop_time": "2021-08-08 21:47:30.257399+00:00",
        },
    ]
    response = client.post(
        "/api/v1/timespans",
        headers=headers,
        json=data,
        auth=("test_user", "test_password"),
    )

    # act
    response = client.get(
        "/api/v1/timespans",
        headers=headers,
        query_string={"to": response.json[0]["stop_time"]},
        auth=("test_user", "test_password"),
    )

    # assert
    assert response.status_code == 200
    response_json = response.get_json()
    assert response_json["request_time"] is not None
    request_time = datetime.strptime(
        response_json["request_time"], "%Y-%m-%d %H:%M:%S.%f"
    )
    assert isinstance(request_time, datetime) is True
    response_data = response_json["data"]
    assert len(response_data) == 1
    assert response_data[0]["id"] == data[0]["id"]
    assert response_data[0]["start_time"] == data[0]["start_time"]
    assert response_data[0]["stop_time"] == data[0]["stop_time"]
    assert response_data[0]["created_at"] is not None
    assert response_data[0]["updated_at"] is not None
