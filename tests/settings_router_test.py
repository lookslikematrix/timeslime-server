"""test settings routes"""
import os
import tempfile
from datetime import datetime
from json import dumps
from uuid import UUID

import pytest

from timeslime_server import create_app
from timeslime_server.model import Setting
from timeslime_server.serializer import SettingSerializer


@pytest.fixture(name="client")
def fixture_client():
    """create test client"""
    db_fd, db_path = tempfile.mkstemp()
    app = create_app({"TESTING": True, "DATABASE": db_path})

    with app.test_client() as client:
        yield client

    os.close(db_fd)
    os.unlink(db_path)


def test_post_setting_no_user_error(client):
    """test POST setting"""
    # arrange & act
    response = client.post("/api/v1/settings")

    # assert
    assert response.status_code == 403
    assert response.get_data() == b"Please provide user credentials with your request"


def test_post_setting_wrong_user_error(client):
    """test POST setting"""
    # arrange & act
    response = client.post("/api/v1/settings", auth=("wrong_user", "password"))

    # assert
    assert response.status_code == 403
    assert response.get_data() == b"User is not authenticated"


def test_post_setting_no_data_error(client):
    """test POST setting"""
    # arrange & act
    response = client.post("/api/v1/settings", auth=("test_user", "test_password"))

    # assert
    assert response.status_code == 400
    assert response.get_data() == b"Content-Type must be application/json"


def test_post_setting_no_json_error(client):
    """test POST setting"""
    # arrange
    headers = {"Content-Type": "application/txt"}
    data = "test"

    # act
    response = client.post(
        "/api/v1/settings",
        headers=headers,
        data=data,
        auth=("test_user", "test_password"),
    )

    # assert
    assert response.status_code == 400
    assert response.get_data() == b"Content-Type must be application/json"


def test_post_setting_wrong_json_error(client):
    """test POST setting"""
    # arrange
    headers = {"Content-Type": "application/json"}
    data = "test"

    # act
    response = client.post(
        "/api/v1/settings",
        headers=headers,
        data=data,
        auth=("test_user", "test_password"),
    )

    # assert
    assert response.status_code == 400


def test_post_setting_correct_json_wrong_object_error(client):
    """test POST setting"""
    # arrange
    headers = {"Content-Type": "application/json"}
    data = dumps({"foo": 2})

    # act
    response = client.post(
        "/api/v1/settings",
        headers=headers,
        data=data,
        auth=("test_user", "test_password"),
    )

    # assert
    assert response.status_code == 400
    assert response.get_data() == b"Could not serialize object"


def test_post_setting_correct_json_correct_object_success(client):
    """test POST setting"""
    # arrange
    headers = {"Content-Type": "application/json"}
    data = {
        "id": "51288003-49dc-4e34-82d4-51f84fd9e59d",
        "key": "test_key",
        "value": "test_value",
    }

    # act
    response = client.post(
        "/api/v1/settings",
        headers=headers,
        json=data,
        auth=("test_user", "test_password"),
    )

    # assert
    assert response.status_code == 200
    response_data = response.get_json()
    assert response_data["id"] == data["id"]
    assert response_data["key"] == data["key"]
    assert response_data["value"] == data["value"]
    assert response_data["created_at"] is not None
    assert response_data["updated_at"] is not None


def test_post_setting_array_with_one_wrong_object_only_right_objects_success(client):
    """test POST setting"""
    # arrange
    headers = {"Content-Type": "application/json"}
    data = [
        {
            "id": "51288003-49dc-4e34-82d4-51f84fd9e59d",
            "value": "test_value_1",
        },
        {
            "id": "51288003-49dc-4e34-82d4-51f84fd9e59b",
            "key": "test_key_2",
            "value": "test_value_2",
        },
    ]

    # act
    response = client.post(
        "/api/v1/settings",
        headers=headers,
        json=data,
        auth=("test_user", "test_password"),
    )

    # assert
    assert response.status_code == 200
    response_data = response.get_json()
    assert len(response_data) == 1
    assert response_data[0]["id"] == data[1]["id"]
    assert response_data[0]["key"] == data[1]["key"]
    assert response_data[0]["value"] == data[1]["value"]
    assert response_data[0]["created_at"] is not None
    assert response_data[0]["updated_at"] is not None


def test_post_setting_array_success(client):
    """test POST setting"""
    # arrange
    headers = {"Content-Type": "application/json"}
    data = [
        {
            "id": "51288003-49dc-4e34-82d4-51f84fd9e59d",
            "key": "test_key_1",
            "value": "test_value_1",
        },
        {
            "id": "51288003-49dc-4e34-82d4-51f84fd9e59b",
            "key": "test_key_2",
            "value": "test_value_2",
        },
    ]

    # act
    response = client.post(
        "/api/v1/settings",
        headers=headers,
        json=data,
        auth=("test_user", "test_password"),
    )

    # assert
    assert response.status_code == 200
    response_data = response.get_json()
    assert len(response_data) == 2
    assert response_data[0]["id"] == data[0]["id"]
    assert response_data[0]["key"] == data[0]["key"]
    assert response_data[0]["value"] == data[0]["value"]
    assert response_data[0]["created_at"] is not None
    assert response_data[0]["updated_at"] is not None

    assert response_data[1]["id"] == data[1]["id"]
    assert response_data[1]["key"] == data[1]["key"]
    assert response_data[1]["value"] == data[1]["value"]
    assert response_data[1]["created_at"] is not None
    assert response_data[1]["updated_at"] is not None


def test_post_setting_array_serialized_success(client):
    """test POST setting"""
    # arrange
    headers = {"Content-Type": "application/json"}
    setting_serializer = SettingSerializer()
    setting_1 = Setting()
    setting_1.id = UUID("51288003-49dc-4e34-82d4-51f84fd9e59d")
    setting_1.key = "test_key_1"
    setting_1.value = "test_value_1"
    setting_2 = Setting()
    setting_2.id = UUID("51288003-49dc-4e34-82d4-51f84fd9e59b")
    setting_2.key = "test_key_2"
    setting_2.value = "test_value_2"

    data = setting_serializer.serialize_list([setting_1, setting_2])

    # act
    response = client.post(
        "/api/v1/settings",
        headers=headers,
        json=data,
        auth=("test_user", "test_password"),
    )

    # assert
    assert response.status_code == 200
    response_data = response.get_json()
    assert len(response_data) == 2
    assert response_data[0]["id"] == data[0]["id"]
    assert response_data[0]["key"] == data[0]["key"]
    assert response_data[0]["value"] == data[0]["value"]
    assert response_data[0]["created_at"] is not None
    assert response_data[0]["updated_at"] is not None

    assert response_data[1]["id"] == data[1]["id"]
    assert response_data[1]["key"] == data[1]["key"]
    assert response_data[1]["value"] == data[1]["value"]
    assert response_data[1]["created_at"] is not None
    assert response_data[1]["updated_at"] is not None


def test_get_settings_no_user_error(client):
    """test GET setting"""
    # arrange & act
    response = client.get("/api/v1/settings")

    # assert
    assert response.status_code == 403
    assert response.get_data() == b"Please provide user credentials with your request"


def test_get_settings_wrong_user_error(client):
    """test GET setting"""
    # arrange & act
    response = client.get("/api/v1/settings", auth=("wrong_user", "password"))

    # assert
    assert response.status_code == 403
    assert response.get_data() == b"User is not authenticated"

def test_get_settings_no_settings_success(client):
    """test GET setting"""
    # arrange
    headers = {
        "Content-Type": "application/json",
    }

    # act
    response = client.get(
        "/api/v1/settings", headers=headers, auth=("test_user", "test_password")
    )

    # assert
    assert response.status_code == 200
    response_json = response.get_json()
    assert response_json["data"] == []
    assert response_json["request_time"] is not None
    request_time = datetime.strptime(
        response_json["request_time"], "%Y-%m-%d %H:%M:%S.%f"
    )
    assert isinstance(request_time, datetime) is True


def test_get_settings_settings_success(client):
    """test GET setting"""
    # arrange
    headers = {"Content-Type": "application/json"}
    data = {
        "id": str(UUID("5128800349dc4e3482d451f84fd9e59d")),
        "key": "test_key",
        "value": "test_value",
    }
    response = client.post(
        "/api/v1/settings",
        headers=headers,
        json=data,
        auth=("test_user", "test_password"),
    )

    # act
    response = client.get(
        "/api/v1/settings", headers=headers, auth=("test_user", "test_password")
    )

    # assert
    assert response.status_code == 200
    response_json = response.get_json()
    assert response_json["request_time"] is not None
    request_time = datetime.strptime(
        response_json["request_time"], "%Y-%m-%d %H:%M:%S.%f"
    )
    assert isinstance(request_time, datetime) is True
    response_data = response_json["data"]
    assert len(response_data) == 1
    assert response_data[0]["id"] == data["id"]
    assert response_data[0]["key"] == data["key"]
    assert response_data[0]["value"] == data["value"]
    assert response_data[0]["created_at"] is not None
    assert response_data[0]["updated_at"] is not None


def test_get_settings_with_wrong_filter_date_settings_success(client):
    """test GET setting"""
    # arrange
    headers = {"Content-Type": "application/json"}
    data = [
        {
            "id": str(UUID("5128800349dc4e3482d451f84fd9e59d")),
            "key": "test_key_1",
            "value": "test_value",
        },
        {
            "id": str(UUID("5128800349dc4e3482d451f84fd9e59b")),
            "key": "test_key_2",
            "value": "test_value",
        },
    ]
    response = client.post(
        "/api/v1/settings",
        headers=headers,
        json=data,
        auth=("test_user", "test_password"),
    )

    # act
    response = client.get(
        "/api/v1/settings",
        headers=headers,
        json={"filter_datetime": "no_time"},
        auth=("test_user", "test_password"),
    )

    # assert
    assert response.status_code == 200
    response_json = response.get_json()
    assert response_json["request_time"] is not None
    request_time = datetime.strptime(
        response_json["request_time"], "%Y-%m-%d %H:%M:%S.%f"
    )
    assert isinstance(request_time, datetime) is True
    response_data = response_json["data"]
    assert len(response_data) == 2
    assert response_data[0]["id"] == data[0]["id"]
    assert response_data[0]["key"] == data[0]["key"]
    assert response_data[0]["value"] == data[0]["value"]
    assert response_data[0]["created_at"] is not None
    assert response_data[0]["updated_at"] is not None
    assert response_data[1]["id"] == data[1]["id"]
    assert response_data[1]["key"] == data[1]["key"]
    assert response_data[1]["value"] == data[1]["value"]
    assert response_data[1]["created_at"] is not None
    assert response_data[1]["updated_at"] is not None


def test_get_settings_with_filter_date_settings_success(client):
    """test GET setting"""
    # arrange
    headers = {"Content-Type": "application/json"}
    data = [
        {
            "id": str(UUID("5128800349dc4e3482d451f84fd9e59d")),
            "key": "test_key_1",
            "value": "test_value",
        },
        {
            "id": str(UUID("5128800349dc4e3482d451f84fd9e59b")),
            "key": "test_key_2",
            "value": "test_value",
        },
    ]
    response = client.post(
        "/api/v1/settings",
        headers=headers,
        json=data,
        auth=("test_user", "test_password"),
    )

    # act
    response = client.get(
        "/api/v1/settings",
        headers=headers,
        json={"filter_datetime": response.json[0]["updated_at"]},
        auth=("test_user", "test_password"),
    )

    # assert
    assert response.status_code == 200
    response_json = response.get_json()
    assert response_json["request_time"] is not None
    request_time = datetime.strptime(
        response_json["request_time"], "%Y-%m-%d %H:%M:%S.%f"
    )
    assert isinstance(request_time, datetime) is True
    response_data = response_json["data"]
    assert len(response_data) == 1
    assert response_data[0]["id"] == data[1]["id"]
    assert response_data[0]["key"] == data[1]["key"]
    assert response_data[0]["value"] == data[1]["value"]
    assert response_data[0]["created_at"] is not None
    assert response_data[0]["updated_at"] is not None
