"""timespan serializer tests"""
import unittest
import uuid
from datetime import datetime

from pytest import raises

from timeslime_server.model import Setting
from timeslime_server.serializer import SettingSerializer


class SettingSerializerTests(unittest.TestCase):
    """test class for the SettingSerializerTests"""

    def test_deserialize_setting_all_is_defined_success(self):
        """test deserialize method"""
        # arrange
        json_string = (
            "{"
            '"id": "51288003-49dc-4e34-82d4-51f84fd9e59d", '
            '"key": "test_key", '
            '"value": "test_value"'
            "}"
        )
        setting_serializer = SettingSerializer()

        # act
        setting = setting_serializer.deserialize(json_string)

        # assert
        self.assertEqual(type(setting.id), uuid.UUID)
        self.assertEqual(str(setting.id), "51288003-49dc-4e34-82d4-51f84fd9e59d")
        self.assertEqual(type(setting.key), str)
        self.assertEqual(setting.key, "test_key")
        self.assertEqual(type(setting.value), str)
        self.assertEqual(setting.value, "test_value")

    def test_deserialize_setting_with_created_and_updated_at(self):
        """test deserialize method"""
        # arrange
        json_string = {
            "id": "51288003-49dc-4e34-82d4-51f84fd9e59d",
            "key": "test_key",
            "value": "test_value",
            "created_at": "2021-08-08 21:47:20.833440",
            "updated_at": "2021-08-08 21:47:20.833440",
        }
        setting_serializer = SettingSerializer()

        # act
        setting = setting_serializer.deserialize(json_string)

        # assert
        self.assertEqual(type(setting.id), uuid.UUID)
        self.assertEqual(str(setting.id), "51288003-49dc-4e34-82d4-51f84fd9e59d")
        self.assertEqual(type(setting.key), str)
        self.assertEqual(setting.key, "test_key")
        self.assertEqual(type(setting.value), str)
        self.assertEqual(setting.value, "test_value")
        self.assertEqual(type(setting.created_at), datetime)
        self.assertEqual(str(setting.created_at), "2021-08-08 21:47:20.833440+00:00")
        self.assertEqual(type(setting.updated_at), datetime)
        self.assertEqual(str(setting.updated_at), "2021-08-08 21:47:20.833440+00:00")

    def test_deserialize_setting_with_different_date_format(self):
        """test deserialize method"""
        # arrange
        json_string = {
            "id": "51288003-49dc-4e34-82d4-51f84fd9e59d",
            "key": "test_key",
            "value": "test_value",
            "created_at": "2021-08-08 21:47:20",
            "updated_at": "2021-08-08 21:47:20",
        }
        setting_serializer = SettingSerializer()

        # act
        setting = setting_serializer.deserialize(json_string)

        # assert
        self.assertEqual(type(setting.id), uuid.UUID)
        self.assertEqual(str(setting.id), "51288003-49dc-4e34-82d4-51f84fd9e59d")
        self.assertEqual(type(setting.key), str)
        self.assertEqual(setting.key, "test_key")
        self.assertEqual(type(setting.value), str)
        self.assertEqual(setting.value, "test_value")
        self.assertEqual(type(setting.created_at), datetime)
        self.assertEqual(str(setting.created_at), "2021-08-08 21:47:20+00:00")
        self.assertEqual(type(setting.updated_at), datetime)
        self.assertEqual(str(setting.updated_at), "2021-08-08 21:47:20+00:00")

    def test_deserialize_setting_with_date_none_string(self):
        """test deserialize method"""
        # arrange
        json_string = {
            "id": "51288003-49dc-4e34-82d4-51f84fd9e59d",
            "key": "test_key",
            "value": "test_value",
            "created_at": "None",
            "updated_at": "None",
        }
        setting_serializer = SettingSerializer()

        # act
        setting = setting_serializer.deserialize(json_string)

        # assert
        self.assertEqual(type(setting.id), uuid.UUID)
        self.assertEqual(str(setting.id), "51288003-49dc-4e34-82d4-51f84fd9e59d")
        self.assertEqual(type(setting.key), str)
        self.assertEqual(setting.key, "test_key")
        self.assertEqual(type(setting.value), str)
        self.assertEqual(setting.value, "test_value")

    def test_deserialize_setting_with_date_none(self):
        """test deserialize method"""
        # arrange
        json_string = {
            "id": "51288003-49dc-4e34-82d4-51f84fd9e59d",
            "key": "test_key",
            "value": "test_value",
            "created_at": None,
            "updated_at": None,
        }
        setting_serializer = SettingSerializer()

        # act
        setting = setting_serializer.deserialize(json_string)

        # assert
        self.assertEqual(type(setting.id), uuid.UUID)
        self.assertEqual(str(setting.id), "51288003-49dc-4e34-82d4-51f84fd9e59d")
        self.assertEqual(type(setting.key), str)
        self.assertEqual(setting.key, "test_key")
        self.assertEqual(type(setting.value), str)
        self.assertEqual(setting.value, "test_value")

    def test_deserialize_setting_wrong_id_error(self):
        """test deserialize method"""
        # arrange
        json_string = '{"id": "test_id", "key": "test_key", "value": "test_value"}'
        setting_serializer = SettingSerializer()

        # act & assert
        with raises(ValueError):
            setting_serializer.deserialize(json_string)

    def test_deserialize_setting_missing_id_success(self):
        """test deserialize method"""
        # arrange
        json_string = '{"key": "test_key", "value": "test_value"}'
        setting_serializer = SettingSerializer()

        # act
        setting = setting_serializer.deserialize(json_string)

        # assert
        self.assertEqual(type(setting.id), uuid.UUID)
        self.assertIsNotNone(setting.id)
        self.assertEqual(type(setting.key), str)
        self.assertEqual(setting.key, "test_key")
        self.assertEqual(type(setting.value), str)
        self.assertEqual(setting.value, "test_value")

    def test_deserialize_setting_without_value_success(self):
        """test deserialize method"""
        # arrange
        json_string = '{"key": "test_key"}'
        setting_serializer = SettingSerializer()

        # act
        setting = setting_serializer.deserialize(json_string)

        # assert
        self.assertEqual(type(setting.id), uuid.UUID)
        self.assertIsNotNone(setting.id)
        self.assertEqual(type(setting.key), str)
        self.assertEqual(setting.key, "test_key")
        self.assertIsNone(setting.value)

    def test_deserialize_setting_missing_key_error(self):
        """test deserialize method"""
        # arrange
        json_string = '{"value": "test_value"}'
        setting_serializer = SettingSerializer()

        # act & assert
        with raises(KeyError):
            setting_serializer.deserialize(json_string)

    def test_deserialize_setting_input_is_dict_success(self):
        """test deserialize method"""
        # arrange
        json_string = {
            "id": "51288003-49dc-4e34-82d4-51f84fd9e59d",
            "key": "test_key",
            "value": "test_value",
        }
        setting_serializer = SettingSerializer()

        # act
        setting = setting_serializer.deserialize(json_string)

        # assert
        self.assertEqual(type(setting.id), uuid.UUID)
        self.assertEqual(str(setting.id), "51288003-49dc-4e34-82d4-51f84fd9e59d")
        self.assertEqual(type(setting.key), str)
        self.assertEqual(setting.key, "test_key")
        self.assertEqual(type(setting.value), str)
        self.assertEqual(setting.value, "test_value")

    def test_deserialize_setting_value_is_none_success(self):
        """test deserialize method"""
        # arrange
        json_string = {
            "id": "51288003-49dc-4e34-82d4-51f84fd9e59d",
            "key": "test_key",
            "value": "None",
        }
        setting_serializer = SettingSerializer()

        # act
        setting = setting_serializer.deserialize(json_string)

        # assert
        self.assertEqual(type(setting.id), uuid.UUID)
        self.assertIsNotNone(setting.id)
        self.assertEqual(type(setting.key), str)
        self.assertEqual(setting.key, "test_key")
        self.assertIsNone(setting.value)

    def test_deserialize_setting_key_is_none_error(self):
        """test deserialize method"""
        # arrange
        json_string = {
            "id": "51288003-49dc-4e34-82d4-51f84fd9e59d",
            "key": "None",
            "value": "None",
        }
        setting_serializer = SettingSerializer()

        # act & assert
        with raises(KeyError):
            setting_serializer.deserialize(json_string)

    def test_serialize_setting_everything_is_none(self):
        """test serialize method"""
        # arrange
        setting = Setting()
        setting_serializer = SettingSerializer()

        # act
        json_string = setting_serializer.serialize(setting)

        # assert
        json_response = {
            "id": str(setting.id),
            "key": setting.key,
            "value": setting.value,
        }
        self.assertEqual(json_string["id"], json_response["id"])
        self.assertEqual(json_string["key"], json_response["key"])
        self.assertEqual(json_string["value"], json_response["value"])
        self.assertIsNotNone(json_string["created_at"])
        self.assertIsNotNone(json_string["updated_at"])

    def test_serialize_setting(self):
        """test serialize method"""
        # arrange
        setting = Setting()
        setting.key = "test_key"
        setting.value = "test_value"
        setting_serializer = SettingSerializer()

        # act
        json_string = setting_serializer.serialize(setting)

        # assert
        json_response = {
            "id": str(setting.id),
            "key": setting.key,
            "value": setting.value,
        }
        self.assertEqual(json_string["id"], json_response["id"])
        self.assertEqual(json_string["key"], json_response["key"])
        self.assertEqual(json_string["value"], json_response["value"])
        self.assertIsNotNone(json_string["created_at"])
        self.assertIsNotNone(json_string["updated_at"])

    def test_serialize_list_empty_settings(self):
        """test serialize_list method"""
        # arrange
        setting_serializer = SettingSerializer()

        # act
        json_string = setting_serializer.serialize_list([])

        # assert
        json_response = []
        self.assertEqual(json_string, json_response)

    def test_serialize_list_correct_settings(self):
        """test serialize_list method"""
        # arrange
        setting = Setting()
        setting.key = "test_key"
        setting.value = "test_value"
        setting_serializer = SettingSerializer()

        # act
        json_string = setting_serializer.serialize_list([setting])

        # assert
        json_response = [
            {"id": str(setting.id), "key": setting.key, "value": setting.value}
        ]
        self.assertEqual(len(json_string), 1)
        self.assertEqual(json_string[0]["id"], json_response[0]["id"])
        self.assertEqual(json_string[0]["key"], json_response[0]["key"])
        self.assertEqual(json_string[0]["value"], json_response[0]["value"])
        self.assertIsNotNone(json_string[0]["created_at"])
        self.assertIsNotNone(json_string[0]["updated_at"])

    def test_serialize_list_wrong_settings(self):
        """test serialize_list method"""
        # arrange
        setting = Setting()
        setting.value = "test_value"
        setting_serializer = SettingSerializer()

        # act
        json_string = setting_serializer.serialize_list([setting])

        # assert
        json_response = [{"id": str(setting.id), "key": None, "value": setting.value}]
        self.assertEqual(len(json_string), 1)
        self.assertEqual(json_string[0]["id"], json_response[0]["id"])
        self.assertEqual(json_string[0]["key"], json_response[0]["key"])
        self.assertEqual(json_string[0]["value"], json_response[0]["value"])
        self.assertIsNotNone(json_string[0]["created_at"])
        self.assertIsNotNone(json_string[0]["updated_at"])

if __name__ == "__main__":
    unittest.main()
