"""database handler tests"""
# pylint: disable=too-many-public-methods
import unittest
from datetime import datetime
from uuid import UUID, uuid4

from peewee import SqliteDatabase
from pytest import raises

from timeslime_server.handler import DatabaseHandler
from timeslime_server.model import Setting, Timespan, User


class DatabaseHandlerTests(unittest.TestCase):
    """test class for the DatabaseHandler"""
    def test_save_timespan_wrong_object(self):
        """test save_timespan method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            database_handler = DatabaseHandler(connection)

            # act
            database_handler.save_timespan(None)

            # assert
            cursor = connection.execute_sql("SELECT count(*) FROM timespans;")
            self.assertEqual(cursor.fetchone()[0], 0)

    def test_save_timespan_no_start_time(self):
        """test save_timespan method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            database_handler = DatabaseHandler(connection)
            timespan = Timespan()

            # act
            database_handler.save_timespan(timespan)

            # assert
            cursor = connection.execute_sql("SELECT count(*) FROM timespans;")
            self.assertEqual(cursor.fetchone()[0], 0)

    def test_save_timespan_only_start_time(self):
        """test save_timespan method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            database_handler = DatabaseHandler(connection)
            timespan = Timespan()
            timespan.user = User()
            timespan.start_time = datetime.now()

            # act
            database_handler.save_timespan(timespan)

            # assert
            cursor = connection.execute_sql("SELECT * FROM timespans;")
            row = cursor.fetchone()
            self.assertEqual(UUID(row[0]), timespan.id)
            self.assertEqual(UUID(row[1]), timespan.user.id)
            self.assertEqual(row[2], str(timespan.start_time))
            self.assertEqual(row[3], None)

    def test_save_timespan_start_time_and_stop_time(self):
        """test save_timespan method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            database_handler = DatabaseHandler(connection)
            timespan = Timespan()
            timespan.user = User()
            timespan.start_time = datetime.now()
            timespan.stop_time = datetime.now()

            # act
            database_handler.save_timespan(timespan)

            # assert
            cursor = connection.execute_sql("SELECT * FROM timespans;")
            row = cursor.fetchone()
            self.assertEqual(UUID(row[0]), timespan.id)
            self.assertEqual(UUID(row[1]), timespan.user.id)
            self.assertEqual(row[2], str(timespan.start_time))
            self.assertEqual(row[3], str(timespan.stop_time))

    def test_save_timespan_already_updated_at_is_updated(self):
        """test save_setting method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            database_handler = DatabaseHandler(connection)
            timespan = Timespan()
            timespan.user = User()
            timespan.id = UUID("e4472559-b7a3-4313-a865-609aebd9458f")
            timespan.start_time = datetime.strptime(
                "2021-10-10 20:00", "%Y-%m-%d %H:%M"
            )
            timespan.created_at = datetime.strptime(
                "2021-10-10 20:00", "%Y-%m-%d %H:%M"
            )
            timespan.updated_at = datetime.strptime(
                "2021-10-10 20:00", "%Y-%m-%d %H:%M"
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("e4472559b7a34313a865609aebd9458f", "{str(timespan.user.id)}",'
                    f'"{timespan.start_time}", NULL,'
                    f'"{timespan.created_at}", "{timespan.updated_at}");'
                )
            )
            connection.commit()
            timespan.stop_time = datetime.strptime("2021-10-10 20:00", "%Y-%m-%d %H:%M")
            timespan.updated_at = datetime.utcnow()

            # act
            database_handler.save_timespan(timespan)

            # assert
            cursor = connection.execute_sql("SELECT * FROM timespans;")
            row = cursor.fetchone()
            self.assertEqual(UUID(row[0]), timespan.id)
            self.assertEqual(UUID(row[1]), timespan.user.id)
            self.assertEqual(row[2], "2021-10-10 20:00:00")
            self.assertEqual(row[3], "2021-10-10 20:00:00")
            self.assertEqual(row[4], "2021-10-10 20:00:00")
            self.assertNotEqual(row[5], "2021-10-10 20:00:00")

    def test_save_timespan_client_updated_at_is_newer_overwrite(self):
        """test save_setting method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            database_handler = DatabaseHandler(connection)
            timespan = Timespan()
            timespan.user = User()
            timespan.id = UUID("e4472559-b7a3-4313-a865-609aebd9458f")
            timespan.start_time = datetime.strptime(
                "2021-10-10 20:00", "%Y-%m-%d %H:%M"
            )
            timespan.created_at = datetime.strptime(
                "2021-10-10 20:00", "%Y-%m-%d %H:%M"
            )
            timespan.updated_at = datetime.strptime(
                "2021-10-10 20:00", "%Y-%m-%d %H:%M"
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("e4472559b7a34313a865609aebd9458f", "{str(timespan.user.id)}",'
                    f'"{timespan.start_time}", NULL,'
                    f'"{timespan.created_at}", "{timespan.updated_at}");'
                )
            )
            connection.commit()
            timespan.stop_time = datetime.strptime("2021-10-10 20:10", "%Y-%m-%d %H:%M")
            timespan.updated_at = datetime.strptime(
                "2021-10-10 20:10", "%Y-%m-%d %H:%M"
            )

            # act
            database_handler.save_timespan(timespan)

            # assert
            cursor = connection.execute_sql("SELECT * FROM timespans;")
            rows = cursor.fetchall()
            self.assertEqual(len(rows), 1)
            row = rows[0]
            self.assertEqual(UUID(row[0]), timespan.id)
            self.assertEqual(UUID(row[1]), timespan.user.id)
            self.assertEqual(row[2], "2021-10-10 20:00:00")
            self.assertEqual(row[3], "2021-10-10 20:10:00")
            self.assertEqual(row[4], "2021-10-10 20:00:00")
            self.assertNotEqual(row[5], "2021-10-10 20:00:00")

    def test_save_timespan_different_dateformats(self):
        """test save_setting method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            database_handler = DatabaseHandler(connection)
            timespan = Timespan()
            timespan.user = User()
            timespan.id = UUID("e4472559-b7a3-4313-a865-609aebd9458f")
            timespan.start_time = datetime.strptime(
                "2021-10-10 20:00", "%Y-%m-%d %H:%M"
            )
            timespan.created_at = datetime.strptime(
                "2021-10-10 20:00", "%Y-%m-%d %H:%M"
            )
            timespan.updated_at = datetime.strptime(
                "2021-10-10 20:00", "%Y-%m-%d %H:%M"
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("e4472559b7a34313a865609aebd9458f", "{str(timespan.user.id)}",'
                    f'"{timespan.start_time}", NULL,'
                    f'"{timespan.created_at}", "{timespan.updated_at}");'
                )
            )
            connection.commit()
            timespan.stop_time = datetime.fromisoformat("2021-10-10 20:10:00+00:00")
            timespan.updated_at = datetime.fromisoformat("2021-10-10 20:10:00+00:00")

            # act
            database_handler.save_timespan(timespan)

            # assert
            cursor = connection.execute_sql("SELECT * FROM timespans;")
            rows = cursor.fetchall()
            self.assertEqual(len(rows), 1)
            row = rows[0]
            self.assertEqual(UUID(row[0]), timespan.id)
            self.assertEqual(UUID(row[1]), timespan.user.id)
            self.assertEqual(row[2], "2021-10-10 20:00:00")
            self.assertEqual(row[3], "2021-10-10 20:10:00+00:00")
            self.assertEqual(row[4], "2021-10-10 20:00:00")
            self.assertNotEqual(row[5], "2021-10-10 20:00:00+00:00")

    def test_save_timespan_client_updated_at_is_older_not_overwrite(self):
        """test save_setting method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            database_handler = DatabaseHandler(connection)
            timespan = Timespan()
            timespan.user = User()
            timespan.id = UUID("e4472559-b7a3-4313-a865-609aebd9458f")
            timespan.start_time = datetime.strptime(
                "2021-10-10 20:00", "%Y-%m-%d %H:%M"
            )
            timespan.created_at = datetime.strptime(
                "2021-10-10 20:00", "%Y-%m-%d %H:%M"
            )
            timespan.updated_at = datetime.strptime(
                "2021-10-10 20:00", "%Y-%m-%d %H:%M"
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("e4472559b7a34313a865609aebd9458f", "{str(timespan.user.id)}",'
                    f'"{timespan.start_time}", NULL,'
                    f'"{timespan.created_at}", "{timespan.updated_at}");'
                )
            )
            connection.commit()
            timespan.stop_time = datetime.strptime("2021-10-10 20:10", "%Y-%m-%d %H:%M")
            timespan.updated_at = datetime.strptime(
                "2021-10-10 19:50", "%Y-%m-%d %H:%M"
            )

            # act
            database_handler.save_timespan(timespan)

            # assert
            cursor = connection.execute_sql("SELECT * FROM timespans;")
            rows = cursor.fetchall()
            self.assertEqual(len(rows), 1)
            row = rows[0]
            self.assertEqual(UUID(row[0]), timespan.id)
            self.assertEqual(UUID(row[1]), timespan.user.id)
            self.assertEqual(row[2], "2021-10-10 20:00:00")
            self.assertEqual(row[3], None)
            self.assertEqual(row[4], "2021-10-10 20:00:00")
            self.assertEqual(row[5], "2021-10-10 20:00:00")

    def test_save_timespan_client_updated_at_is_identical_not_overwrite(self):
        """test save_setting method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            database_handler = DatabaseHandler(connection)
            timespan = Timespan()
            timespan.user = User()
            timespan.id = UUID("e4472559-b7a3-4313-a865-609aebd9458f")
            timespan.start_time = datetime.strptime(
                "2021-10-10 20:00", "%Y-%m-%d %H:%M"
            )
            timespan.created_at = datetime.strptime(
                "2021-10-10 20:00", "%Y-%m-%d %H:%M"
            )
            timespan.updated_at = datetime.strptime(
                "2021-10-10 20:00", "%Y-%m-%d %H:%M"
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("e4472559b7a34313a865609aebd9458f", "{str(timespan.user.id)}",'
                    f'"{timespan.start_time}", NULL,'
                    f'"{timespan.created_at}", "{timespan.updated_at}");'
                )
            )
            connection.commit()

            # act
            database_handler.save_timespan(timespan)

            # assert
            cursor = connection.execute_sql("SELECT * FROM timespans;")
            rows = cursor.fetchall()
            self.assertEqual(len(rows), 1)
            row = rows[0]
            self.assertEqual(UUID(row[0]), timespan.id)
            self.assertEqual(UUID(row[1]), timespan.user.id)
            self.assertEqual(row[2], "2021-10-10 20:00:00")
            self.assertEqual(row[3], None)
            self.assertEqual(row[4], "2021-10-10 20:00:00")
            self.assertEqual(row[5], "2021-10-10 20:00:00")

    def test_save_timespans_no_array(self):
        """test save_setting method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            database_handler = DatabaseHandler(connection)

            # act
            database_handler.save_timespans(None)

            # assert
            cursor = connection.execute_sql("SELECT count(*) FROM timespans;")
            self.assertEqual(cursor.fetchone()[0], 0)

    def test_save_timespans_array(self):
        """test save_setting method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            database_handler = DatabaseHandler(connection)
            user = User()
            timespan1 = Timespan()
            timespan1.user = user
            timespan1.id = UUID("e4472559b7a34313a865609aebd9458f")
            timespan1.start_time = datetime.strptime(
                "2021-10-10 20:00", "%Y-%m-%d %H:%M"
            )
            timespan1.stop_time = datetime.strptime(
                "2021-10-10 20:10", "%Y-%m-%d %H:%M"
            )
            timespan2 = Timespan()
            timespan2.user = user
            timespan2.start_time = datetime.strptime(
                "2021-10-10 21:00", "%Y-%m-%d %H:%M"
            )
            timespan2.stop_time = datetime.strptime(
                "2021-10-10 21:10", "%Y-%m-%d %H:%M"
            )
            timespans = []
            timespans.append(timespan1)
            timespans.append(timespan2)
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("e4472559b7a34313a865609aebd9458f",'
                    f'"{str(timespan1.user.id)}",'
                    f'"{timespan1.start_time}", "{timespan1.stop_time}",'
                    f'"{timespan1.created_at}", "{timespan1.updated_at}");'
                )
            )

            # act
            database_handler.save_timespans(timespans)

            # assert
            cursor = connection.execute_sql("SELECT * FROM timespans;")
            row = cursor.fetchall()
            self.assertEqual(len(row), 2)
            self.assertEqual(UUID(row[0][0]), timespan1.id)
            self.assertEqual(UUID(row[0][1]), timespan1.user.id)
            self.assertEqual(row[0][2], "2021-10-10 20:00:00")
            self.assertEqual(row[0][3], "2021-10-10 20:10:00")
            self.assertEqual(UUID(row[1][0]), timespan2.id)
            self.assertEqual(UUID(row[1][1]), timespan2.user.id)
            self.assertEqual(row[1][2], "2021-10-10 21:00:00")
            self.assertEqual(row[1][3], "2021-10-10 21:10:00")

    def test_save_timespans_array_existing_element_overwrite(self):
        """test save_setting method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            database_handler = DatabaseHandler(connection)
            user = User()
            timespan1 = Timespan()
            timespan1.user = user
            timespan1.start_time = datetime.strptime(
                "2021-10-10 20:00", "%Y-%m-%d %H:%M"
            )
            timespan1.stop_time = datetime.strptime(
                "2021-10-10 20:10", "%Y-%m-%d %H:%M"
            )
            timespan2 = Timespan()
            timespan2.user = user
            timespan2.start_time = datetime.strptime(
                "2021-10-10 21:00", "%Y-%m-%d %H:%M"
            )
            timespan2.stop_time = datetime.strptime(
                "2021-10-10 21:10", "%Y-%m-%d %H:%M"
            )
            timespans = []
            timespans.append(timespan1)
            timespans.append(timespan2)

            # act
            database_handler.save_timespans(timespans)

            # assert
            cursor = connection.execute_sql("SELECT * FROM timespans;")
            row = cursor.fetchall()
            self.assertEqual(len(row), 2)
            self.assertEqual(UUID(row[0][0]), timespan1.id)
            self.assertEqual(UUID(row[0][1]), timespan1.user.id)
            self.assertEqual(row[0][2], "2021-10-10 20:00:00")
            self.assertEqual(row[0][3], "2021-10-10 20:10:00")
            self.assertEqual(UUID(row[1][0]), timespan2.id)
            self.assertEqual(UUID(row[1][1]), timespan2.user.id)
            self.assertEqual(row[1][2], "2021-10-10 21:00:00")
            self.assertEqual(row[1][3], "2021-10-10 21:10:00")

    def test_get_timespans_timespans(self):
        """test get_timespans method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            database_handler = DatabaseHandler(connection)
            uuid = "3c8116631cf8495d8267ca18abcd1dac"
            connection.execute_sql(
                (
                    "INSERT INTO users "
                    f'VALUES ("{uuid}", "test_user",'
                    '"$2b$12$ipvsDZVBzSvEr/8qKljcPeRJ5bR3EgkRjzpMkOqgbkg2ugQLqrMza",'
                    f' "{datetime.now()}", "{datetime.now()}");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("{uuid4()}", "{uuid}", "{datetime.now()}", "{datetime.now()}",'
                    f' "{datetime.now()}", "{datetime.now()}");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("{uuid4()}", "{uuid}","{datetime.now()}", "{datetime.now()}",'
                    f' "{datetime.now()}", "{datetime.now()}");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("{uuid4()}", "{uuid}","{datetime.now()}", "{datetime.now()}",'
                    f' "{datetime.now()}", "{datetime.now()}");'
                )
            )
            connection.commit()

            # act
            timespans = database_handler.get_timespans(UUID(uuid))

            # assert
            self.assertEqual(len(timespans), 3)

    def test_get_timespans_no_timespans(self):
        """test get_timespans method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            database_handler = DatabaseHandler(connection)

            # act
            timespans = database_handler.get_timespans(uuid4())

            # assert
            self.assertEqual(len(timespans), 0)

    def test_get_timespans_no_stop_time(self):
        """test get_timespans method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            database_handler = DatabaseHandler(connection)
            uuid = "3c8116631cf8495d8267ca18abcd1dac"
            connection.execute_sql(
                (
                    "INSERT INTO users "
                    f'VALUES ("{uuid}", "test_user",'
                    '"$2b$12$ipvsDZVBzSvEr/8qKljcPeRJ5bR3EgkRjzpMkOqgbkg2ugQLqrMza",'
                    f' "{datetime.now()}", "{datetime.now()}");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("{uuid4()}", "{uuid}", "{datetime.now()}", "None",'
                    f' "{datetime.now()}", "{datetime.now()}");'
                )
            )
            connection.commit()

            # act
            timespans = database_handler.get_timespans(UUID(uuid))

            # assert
            self.assertEqual(len(timespans), 1)

    def test_get_timespans_with_datetime_filtered_response(self):
        """test get_timespans method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            database_handler = DatabaseHandler(connection)
            uuid = "3c8116631cf8495d8267ca18abcd1dac"
            connection.execute_sql(
                (
                    "INSERT INTO users "
                    f'VALUES ("{uuid}", "test_user",'
                    '"$2b$12$ipvsDZVBzSvEr/8qKljcPeRJ5bR3EgkRjzpMkOqgbkg2ugQLqrMza",'
                    f' "{datetime.now()}", "{datetime.now()}");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("{uuid4()}", "{uuid}", "{datetime.now()}", "{datetime.now()}",'
                    ' "2021-10-18 16:20", "2021-10-18 16:20");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("{uuid4()}", "{uuid}", "{datetime.now()}", "{datetime.now()}",'
                    ' "2021-10-18 16:20", "2021-10-18 16:25");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("{uuid4()}", "{uuid}", "{datetime.now()}", "{datetime.now()}",'
                    ' "2021-10-18 16:20", "2021-10-18 16:30");'
                )
            )
            connection.commit()
            last_sync_datetime = datetime(2021, 10, 18, 16, 25)

            # act
            timespans = database_handler.get_timespans(UUID(uuid), last_sync_datetime)

            # assert
            self.assertEqual(len(timespans), 1)
            timespan = timespans[0]
            self.assertEqual(timespan.created_at, "2021-10-18 16:20")
            self.assertEqual(timespan.updated_at, "2021-10-18 16:30")

    def test_get_timespans_with_updated_at_and_start_time_filtered_response(self):
        """test get_timespans method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            database_handler = DatabaseHandler(connection)
            uuid = "3c8116631cf8495d8267ca18abcd1dac"
            connection.execute_sql(
                (
                    "INSERT INTO users "
                    f'VALUES ("{uuid}", "test_user",'
                    '"$2b$12$ipvsDZVBzSvEr/8qKljcPeRJ5bR3EgkRjzpMkOqgbkg2ugQLqrMza",'
                    f' "{datetime.now()}", "{datetime.now()}");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("{uuid4()}", "{uuid}", "2021-10-18 16:20", "{datetime.now()}",'
                    ' "2021-10-18 16:20", "2021-10-18 16:20");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("{uuid4()}", "{uuid}", "2021-10-18 16:20", "{datetime.now()}",'
                    ' "2021-10-18 16:20", "2021-10-18 16:25");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("{uuid4()}", "{uuid}", "2022-05-30 16:20", "{datetime.now()}",'
                    ' "2021-10-18 16:20", "2021-10-18 16:15");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("{uuid4()}", "{uuid}", "2022-05-30 16:20", "{datetime.now()}",'
                    ' "2021-10-18 16:20", "2021-10-18 16:30");'
                )
            )
            connection.commit()
            updated_at = datetime(2021, 10, 18, 16, 20)
            start_time = datetime(2022, 5, 29)

            # act
            timespans = database_handler.get_timespans(
                UUID(uuid), updated_at, start_time
            )

            # assert
            self.assertEqual(len(timespans), 1)
            timespan = timespans[0]
            self.assertEqual(timespan.start_time, "2022-05-30 16:20")
            self.assertEqual(timespan.created_at, "2021-10-18 16:20")
            self.assertEqual(timespan.updated_at, "2021-10-18 16:30")

    def test_get_timespans_with_start_time_filtered_response(self):
        """test get_timespans method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            database_handler = DatabaseHandler(connection)
            uuid = "3c8116631cf8495d8267ca18abcd1dac"
            connection.execute_sql(
                (
                    "INSERT INTO users "
                    f'VALUES ("{uuid}", "test_user",'
                    '"$2b$12$ipvsDZVBzSvEr/8qKljcPeRJ5bR3EgkRjzpMkOqgbkg2ugQLqrMza",'
                    f' "{datetime.now()}", "{datetime.now()}");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("{uuid4()}", "{uuid}", "2021-10-18 16:20", "{datetime.now()}",'
                    ' "2021-10-18 16:20", "2021-10-18 16:20");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("{uuid4()}", "{uuid}", "2021-10-18 16:20", "{datetime.now()}",'
                    ' "2021-10-18 16:20", "2021-10-18 16:25");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("{uuid4()}", "{uuid}", "2022-05-30 16:20", "{datetime.now()}",'
                    ' "2021-10-18 16:20", "2021-10-18 16:15");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("{uuid4()}", "{uuid}", "2022-05-30 16:22", "{datetime.now()}",'
                    ' "2021-10-18 16:20", "2021-10-18 16:30");'
                )
            )
            connection.commit()
            start_time = datetime(2022, 5, 29)

            # act
            timespans = database_handler.get_timespans(UUID(uuid), None, start_time)

            # assert
            self.assertEqual(len(timespans), 2)
            timespan = timespans[0]
            self.assertEqual(timespan.start_time, "2022-05-30 16:20")
            self.assertEqual(timespan.created_at, "2021-10-18 16:20")
            self.assertEqual(timespan.updated_at, "2021-10-18 16:15")
            timespan = timespans[1]
            self.assertEqual(timespan.start_time, "2022-05-30 16:22")
            self.assertEqual(timespan.created_at, "2021-10-18 16:20")
            self.assertEqual(timespan.updated_at, "2021-10-18 16:30")

    def test_get_timespans_with_to_date_filtered_response(self):
        """test get_timespans method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            database_handler = DatabaseHandler(connection)
            uuid = "3c8116631cf8495d8267ca18abcd1dac"
            connection.execute_sql(
                (
                    "INSERT INTO users "
                    f'VALUES ("{uuid}", "test_user",'
                    '"$2b$12$ipvsDZVBzSvEr/8qKljcPeRJ5bR3EgkRjzpMkOqgbkg2ugQLqrMza",'
                    f' "{datetime.now()}", "{datetime.now()}");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("{uuid4()}", "{uuid}", "2021-10-18 15:20", "2021-10-18 16:00",'
                    ' "2021-10-18 16:20", "2021-10-18 16:20");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("{uuid4()}", "{uuid}", "2021-10-18 16:00", "2021-10-18 16:20",'
                    ' "2021-10-18 16:20", "2021-10-18 16:25");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("{uuid4()}", "{uuid}", "2022-05-30 16:20", "2021-10-18 17:00",'
                    ' "2021-10-18 16:20", "2021-10-18 16:15");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("{uuid4()}", "{uuid}", "2022-05-30 17:00", "2021-10-18 17:20",'
                    ' "2021-10-18 16:20", "2021-10-18 16:30");'
                )
            )
            connection.commit()
            to_date = datetime(2021, 10, 18, 16)

            # act
            timespans = database_handler.get_timespans(UUID(uuid), None, None, to_date)

            # assert
            self.assertEqual(len(timespans), 1)
            timespan = timespans[0]
            self.assertEqual(timespan.start_time, "2021-10-18 15:20")
            self.assertEqual(timespan.stop_time, "2021-10-18 16:00")
            self.assertEqual(timespan.created_at, "2021-10-18 16:20")
            self.assertEqual(timespan.updated_at, "2021-10-18 16:20")

    def test_get_timespans_with_updated_at_and_to_date_filtered_response(self):
        """test get_timespans method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            database_handler = DatabaseHandler(connection)
            uuid = "3c8116631cf8495d8267ca18abcd1dac"
            connection.execute_sql(
                (
                    "INSERT INTO users "
                    f'VALUES ("{uuid}", "test_user",'
                    '"$2b$12$ipvsDZVBzSvEr/8qKljcPeRJ5bR3EgkRjzpMkOqgbkg2ugQLqrMza",'
                    f' "{datetime.now()}", "{datetime.now()}");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("{uuid4()}", "{uuid}", "2021-10-18 15:20", "2021-10-18 16:00",'
                    ' "2021-10-18 16:20", "2021-10-18 16:20");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("{uuid4()}", "{uuid}", "2021-10-18 16:00", "2021-10-18 16:20",'
                    ' "2021-10-18 16:20", "2021-10-18 16:25");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("{uuid4()}", "{uuid}", "2021-10-18 16:20", "2021-10-18 17:00",'
                    ' "2021-10-18 16:20", "2021-10-18 16:15");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("{uuid4()}", "{uuid}", "2021-10-18 17:00", "2021-10-18 17:20",'
                    ' "2021-10-18 16:20", "2021-10-18 16:30");'
                )
            )
            connection.commit()
            updated_at = datetime(2021, 10, 18, 16, 25)
            to_date = datetime(2021, 10, 18, 17, 20)

            # act
            timespans = database_handler.get_timespans(
                UUID(uuid), updated_at, None, to_date
            )

            # assert
            self.assertEqual(len(timespans), 1)
            timespan = timespans[0]
            self.assertEqual(timespan.start_time, "2021-10-18 17:00")
            self.assertEqual(timespan.stop_time, "2021-10-18 17:20")
            self.assertEqual(timespan.created_at, "2021-10-18 16:20")
            self.assertEqual(timespan.updated_at, "2021-10-18 16:30")

    def test_get_timespans_with_from_and_to_date_filtered_response(self):
        """test get_timespans method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            database_handler = DatabaseHandler(connection)
            uuid = "3c8116631cf8495d8267ca18abcd1dac"
            connection.execute_sql(
                (
                    "INSERT INTO users "
                    f'VALUES ("{uuid}", "test_user",'
                    '"$2b$12$ipvsDZVBzSvEr/8qKljcPeRJ5bR3EgkRjzpMkOqgbkg2ugQLqrMza",'
                    f' "{datetime.now()}", "{datetime.now()}");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("{uuid4()}", "{uuid}", "2021-10-18 15:20", "2021-10-18 16:00",'
                    ' "2021-10-18 16:20", "2021-10-18 16:20");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("{uuid4()}", "{uuid}", "2021-10-18 16:05", "2021-10-18 16:20",'
                    ' "2021-10-18 16:20", "2021-10-18 16:25");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("{uuid4()}", "{uuid}", "2021-10-18 16:20", "2021-10-18 17:00",'
                    ' "2021-10-18 16:20", "2021-10-18 16:15");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("{uuid4()}", "{uuid}", "2022-05-30 17:00", "2021-10-18 17:20",'
                    ' "2021-10-18 16:20", "2021-10-18 16:30");'
                )
            )
            connection.commit()
            from_date = datetime(2021, 10, 18, 16)
            to_date = datetime(2021, 10, 18, 17)

            # act
            timespans = database_handler.get_timespans(
                UUID(uuid), None, from_date, to_date
            )

            # assert
            self.assertEqual(len(timespans), 2)
            timespan = timespans[0]
            self.assertEqual(timespan.start_time, "2021-10-18 16:05")
            self.assertEqual(timespan.stop_time, "2021-10-18 16:20")
            self.assertEqual(timespan.created_at, "2021-10-18 16:20")
            self.assertEqual(timespan.updated_at, "2021-10-18 16:25")
            timespan = timespans[1]
            self.assertEqual(timespan.start_time, "2021-10-18 16:20")
            self.assertEqual(timespan.stop_time, "2021-10-18 17:00")
            self.assertEqual(timespan.created_at, "2021-10-18 16:20")
            self.assertEqual(timespan.updated_at, "2021-10-18 16:15")

    def test_get_timespans_with_all_defined_filtered_response(self):
        """test get_timespans method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            database_handler = DatabaseHandler(connection)
            uuid = "3c8116631cf8495d8267ca18abcd1dac"
            connection.execute_sql(
                (
                    "INSERT INTO users "
                    f'VALUES ("{uuid}", "test_user",'
                    '"$2b$12$ipvsDZVBzSvEr/8qKljcPeRJ5bR3EgkRjzpMkOqgbkg2ugQLqrMza",'
                    f' "{datetime.now()}", "{datetime.now()}");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("{uuid4()}", "{uuid}", "2021-10-18 15:20", "2021-10-18 16:00",'
                    ' "2021-10-18 16:20", "2021-10-18 16:20");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("{uuid4()}", "{uuid}", "2021-10-18 16:05", "2021-10-18 16:20",'
                    ' "2021-10-18 16:20", "2021-10-18 16:25");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("{uuid4()}", "{uuid}", "2021-10-18 16:20", "2021-10-18 17:00",'
                    ' "2021-10-18 16:20", "2021-10-18 16:15");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("{uuid4()}", "{uuid}", "2022-05-30 17:00", "2021-10-18 17:20",'
                    ' "2021-10-18 16:20", "2021-10-18 16:30");'
                )
            )
            connection.commit()
            from_date = datetime(2021, 10, 18, 16)
            to_date = datetime(2021, 10, 18, 17)
            updated_at = datetime(2021, 10, 18, 16, 20)

            # act
            timespans = database_handler.get_timespans(
                UUID(uuid), updated_at, from_date, to_date
            )

            # assert
            self.assertEqual(len(timespans), 1)
            timespan = timespans[0]
            self.assertEqual(timespan.start_time, "2021-10-18 16:05")
            self.assertEqual(timespan.stop_time, "2021-10-18 16:20")
            self.assertEqual(timespan.created_at, "2021-10-18 16:20")
            self.assertEqual(timespan.updated_at, "2021-10-18 16:25")

    def test_get_timespans_only_from_authenticated_user(self):
        """test get_timespans method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            database_handler = DatabaseHandler(connection)
            uuid = "3c8116631cf8495d8267ca18abcd1dac"
            connection.execute_sql(
                (
                    "INSERT INTO users "
                    f'VALUES ("{uuid}", "test_user",'
                    '"$2b$12$ipvsDZVBzSvEr/8qKljcPeRJ5bR3EgkRjzpMkOqgbkg2ugQLqrMza",'
                    f' "{datetime.now()}", "{datetime.now()}");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("{uuid4()}", "{uuid4()}", "{datetime.now()}", "{datetime.now()}",'
                    f' "{datetime.now()}", "{datetime.now()}");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("{uuid4()}", "{uuid}","{datetime.now()}", "{datetime.now()}",'
                    f' "{datetime.now()}", "{datetime.now()}");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO timespans "
                    f'VALUES ("{uuid4()}", "{uuid}","{datetime.now()}", "{datetime.now()}",'
                    f' "{datetime.now()}", "{datetime.now()}");'
                )
            )
            connection.commit()

            # act
            timespans = database_handler.get_timespans(UUID(uuid))

            # assert
            self.assertEqual(len(timespans), 2)

    def test_save_setting_wrong_object(self):
        """test save_setting method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            database_handler = DatabaseHandler(connection)

            # act
            database_handler.save_setting(None)

            # assert
            cursor = connection.execute_sql("SELECT count(*) FROM settings;")
            self.assertEqual(cursor.fetchone()[0], 0)

    def test_save_setting_no_key(self):
        """test save_setting method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            database_handler = DatabaseHandler(connection)
            setting = Setting()

            # act
            database_handler.save_setting(setting)

            # assert
            cursor = connection.execute_sql("SELECT count(*) FROM settings;")
            self.assertEqual(cursor.fetchone()[0], 0)

    def test_save_setting_only_key(self):
        """test save_setting method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            database_handler = DatabaseHandler(connection)
            setting = Setting()
            setting.user = User()
            setting.key = "test_key"

            # act
            database_handler.save_setting(setting)

            # assert
            cursor = connection.execute_sql("SELECT * FROM settings;")
            row = cursor.fetchone()
            self.assertEqual(UUID(row[0]), setting.id)
            self.assertEqual(UUID(row[1]), setting.user.id)
            self.assertEqual(row[2], setting.key)
            self.assertEqual(row[3], None)

    def test_save_setting_key_exists_already_overwrite(self):
        """test save_setting method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            database_handler = DatabaseHandler(connection)
            user = User()
            setting = Setting()
            setting.user = user
            setting.key = "test_key"
            setting.value = "test_value_old"
            setting.created_at = datetime.strptime("2021-10-10 20:00", "%Y-%m-%d %H:%M")
            setting.updated_at = datetime.strptime("2021-10-10 20:00", "%Y-%m-%d %H:%M")
            connection.execute_sql(
                (
                    "INSERT INTO settings "
                    f'VALUES ("{setting.id}", "{setting.user.id}", "{setting.key}",'
                    f'"{setting.value}","{setting.created_at}", "{setting.updated_at}");'
                )
            )
            connection.commit()
            setting = Setting()
            setting.user = user
            setting.key = "test_key"
            setting.value = "test_value_new"

            # act
            database_handler.save_setting(setting)

            # assert
            cursor = connection.execute_sql("SELECT * FROM settings;")
            row = cursor.fetchone()
            self.assertEqual(UUID(row[0]), setting.id)
            self.assertEqual(UUID(row[1]), setting.user.id)
            self.assertEqual(row[2], setting.key)
            self.assertEqual(row[3], setting.value)
            self.assertEqual(row[4], "2021-10-10 20:00:00")
            self.assertNotEqual(row[5], "2021-10-10 20:00:00")

    def test_save_setting_key_and_value(self):
        """test save_setting method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            database_handler = DatabaseHandler(connection)
            setting = Setting()
            setting.user = User()
            setting.key = "test_key"
            setting.value = "test_value"

            # act
            database_handler.save_setting(setting)

            # assert
            cursor = connection.execute_sql("SELECT * FROM settings;")
            row = cursor.fetchone()
            self.assertEqual(UUID(row[0]), setting.id)
            self.assertEqual(UUID(row[1]), setting.user.id)
            self.assertEqual(row[2], setting.key)
            self.assertEqual(row[3], setting.value)

    def test_save_setting_client_updated_at_is_newer_overwrite(self):
        """test save_setting method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            database_handler = DatabaseHandler(connection)
            user = User()
            setting = Setting()
            setting.user = user
            setting.key = "test_key"
            setting.value = "test_value_old"
            setting.created_at = datetime.strptime("2021-10-10 20:00", "%Y-%m-%d %H:%M")
            setting.updated_at = datetime.strptime("2021-10-10 20:00", "%Y-%m-%d %H:%M")
            connection.execute_sql(
                (
                    "INSERT INTO settings "
                    f'VALUES ("{setting.id}", "{setting.user.id}", "{setting.key}",'
                    f'"{setting.value}","{setting.created_at}", "{setting.updated_at}");'
                )
            )
            connection.commit()
            setting = Setting()
            setting.user = user
            setting.key = "test_key"
            setting.value = "test_value_new"
            setting.updated_at = datetime.strptime("2021-10-10 20:10", "%Y-%m-%d %H:%M")

            # act
            database_handler.save_setting(setting)

            # assert
            cursor = connection.execute_sql("SELECT * FROM settings;")
            row = cursor.fetchone()
            self.assertEqual(UUID(row[0]), setting.id)
            self.assertEqual(UUID(row[1]), setting.user.id)
            self.assertEqual(row[2], setting.key)
            self.assertEqual(row[3], setting.value)
            self.assertEqual(row[4], "2021-10-10 20:00:00")
            self.assertNotEqual(row[5], "2021-10-10 20:00:00")

    def test_save_setting_client_updated_at_is_older_not_overwrite(self):
        """test save_setting method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            database_handler = DatabaseHandler(connection)
            user = User()
            setting = Setting()
            setting.user = user
            setting.key = "test_key"
            setting.value = "test_value_old"
            setting.created_at = datetime.strptime("2021-10-10 20:00", "%Y-%m-%d %H:%M")
            setting.updated_at = datetime.strptime("2021-10-10 20:00", "%Y-%m-%d %H:%M")
            connection.execute_sql(
                (
                    "INSERT INTO settings "
                    f'VALUES ("{setting.id}", "{setting.user.id}", "{setting.key}",'
                    f'"{setting.value}","{setting.created_at}", "{setting.updated_at}");'
                )
            )
            connection.commit()
            setting = Setting()
            setting.user = user
            setting.key = "test_key"
            setting.value = "test_value_new"
            setting.updated_at = datetime.strptime("2021-10-10 19:50", "%Y-%m-%d %H:%M")

            # act
            database_handler.save_setting(setting)

            # assert
            cursor = connection.execute_sql("SELECT * FROM settings;")
            row = cursor.fetchone()
            self.assertEqual(UUID(row[0]), setting.id)
            self.assertEqual(UUID(row[1]), setting.user.id)
            self.assertEqual(row[2], setting.key)
            self.assertEqual(row[3], "test_value_old")
            self.assertEqual(row[4], "2021-10-10 20:00:00")
            self.assertEqual(row[5], "2021-10-10 20:00:00")

    def test_save_setting_client_updated_at_is_identical_not_overwrite(self):
        """test save_setting method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            database_handler = DatabaseHandler(connection)
            setting = Setting()
            setting.user = User()
            setting.key = "test_key"
            setting.value = "test_value_old"
            setting.created_at = datetime.strptime("2021-10-10 20:00", "%Y-%m-%d %H:%M")
            setting.updated_at = datetime.strptime("2021-10-10 20:00", "%Y-%m-%d %H:%M")
            connection.execute_sql(
                (
                    "INSERT INTO settings "
                    f'VALUES ("{setting.id}", "{setting.user.id}", "{setting.key}",'
                    f'"{setting.value}","{setting.created_at}", "{setting.updated_at}");'
                )
            )
            connection.commit()

            # act
            database_handler.save_setting(setting)

            # assert
            cursor = connection.execute_sql("SELECT * FROM settings;")
            row = cursor.fetchone()
            self.assertEqual(UUID(row[0]), setting.id)
            self.assertEqual(UUID(row[1]), setting.user.id)
            self.assertEqual(row[2], setting.key)
            self.assertEqual(row[3], "test_value_old")
            self.assertEqual(row[4], "2021-10-10 20:00:00")
            self.assertEqual(row[5], "2021-10-10 20:00:00")

    def test_save_settings_no_array(self):
        """test save_setting method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            database_handler = DatabaseHandler(connection)

            # act
            database_handler.save_settings(None)

            # assert
            cursor = connection.execute_sql("SELECT count(*) FROM settings;")
            self.assertEqual(cursor.fetchone()[0], 0)

    def test_save_settings_settings_exists_array(self):
        """test save_setting method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            database_handler = DatabaseHandler(connection)
            user = User()
            setting1 = Setting()
            setting1.user = user
            setting1.key = "test_key_1"
            setting1.value = "test_value_1_old"
            setting2 = Setting()
            setting2.user = user
            setting2.key = "test_key_2"
            setting2.value = "test_value_2_old"
            old_date = datetime(year=2020, month=1, day=1)
            connection.execute_sql(
                (
                    "INSERT INTO settings "
                    "VALUES "
                    f'("{setting1.id}", "{user.id}", "{setting1.key}", "{setting1.value}",'
                    f'"{old_date}", "{old_date}"),'
                    f'("{setting2.id}", "{user.id}", "{setting2.key}", "{setting2.value}",'
                    f'"{old_date}", "{old_date}")'
                )
            )
            connection.commit()
            setting1.value = "test_value_1_new"
            setting1.updated_at = datetime.now()
            setting2.value = "test_value_2_new"
            setting2.updated_at = datetime.now()

            # act
            database_handler.save_settings([setting1, setting2])

            # assert
            cursor = connection.execute_sql("SELECT * FROM settings;")
            row = cursor.fetchall()
            self.assertEqual(len(row), 2)
            self.assertEqual(UUID(row[0][0]), setting1.id)
            self.assertEqual(UUID(row[0][1]), setting1.user.id)
            self.assertEqual(row[0][2], setting1.key)
            self.assertEqual(row[0][3], setting1.value)
            self.assertEqual(UUID(row[1][0]), setting2.id)
            self.assertEqual(UUID(row[1][1]), setting2.user.id)
            self.assertEqual(row[1][2], setting2.key)
            self.assertEqual(row[1][3], setting2.value)

    def test_save_settings_array(self):
        """test save_setting method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            database_handler = DatabaseHandler(connection)
            user = User()
            setting1 = Setting()
            setting1.user = user
            setting1.key = "test_key_1"
            setting1.value = "test_value_1"
            setting2 = Setting()
            setting2.user = user
            setting2.key = "test_key_2"
            setting2.value = "test_value_2"
            settings = []
            settings.append(setting1)
            settings.append(setting2)

            # act
            database_handler.save_settings(settings)

            # assert
            cursor = connection.execute_sql("SELECT * FROM settings;")
            row = cursor.fetchall()
            self.assertEqual(UUID(row[0][0]), setting1.id)
            self.assertEqual(UUID(row[0][1]), setting1.user.id)
            self.assertEqual(row[0][2], setting1.key)
            self.assertEqual(row[0][3], setting1.value)
            self.assertEqual(UUID(row[1][0]), setting2.id)
            self.assertEqual(UUID(row[1][1]), setting1.user.id)
            self.assertEqual(row[1][2], setting2.key)
            self.assertEqual(row[1][3], setting2.value)

    def test_get_settings_settings(self):
        """test get_settings method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            database_handler = DatabaseHandler(connection)
            uuid = "3c8116631cf8495d8267ca18abcd1dac"
            connection.execute_sql(
                (
                    "INSERT INTO users "
                    f'VALUES ("{uuid}", "test_user",'
                    '"$2b$12$ipvsDZVBzSvEr/8qKljcPeRJ5bR3EgkRjzpMkOqgbkg2ugQLqrMza",'
                    f' "{datetime.now()}", "{datetime.now()}");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO settings "
                    f'VALUES ("{uuid4()}", "{uuid}", "test_key_1", "test_value_1",'
                    f'"{datetime.now()}", "{datetime.now()}");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO settings "
                    f'VALUES ("{uuid4()}", "{uuid}", "test_key_2", "test_value_2",'
                    f'"{datetime.now()}", "{datetime.now()}");'
                )
            )
            connection.commit()

            # act
            settings = database_handler.get_settings(UUID(uuid))

            # assert
            self.assertEqual(len(settings), 2)
            self.assertEqual(settings[0].key, "test_key_1")
            self.assertEqual(settings[0].value, "test_value_1")
            self.assertEqual(settings[1].key, "test_key_2")
            self.assertEqual(settings[1].value, "test_value_2")

    def test_get_settings_no_settings(self):
        """test get_settings method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            database_handler = DatabaseHandler(connection)

            # act
            settings = database_handler.get_settings(uuid4())

            # assert
            self.assertEqual(len(settings), 0)

    def test_get_settings_with_datetime_filtered_response(self):
        """test get_settings method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            database_handler = DatabaseHandler(connection)
            uuid = "3c8116631cf8495d8267ca18abcd1dac"
            connection.execute_sql(
                (
                    "INSERT INTO users "
                    f'VALUES ("{uuid}", "test_user",'
                    '"$2b$12$ipvsDZVBzSvEr/8qKljcPeRJ5bR3EgkRjzpMkOqgbkg2ugQLqrMza",'
                    f' "{datetime.now()}", "{datetime.now()}");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO settings "
                    f'VALUES ("{uuid4()}", "{uuid}", "test_key_1", "test_value_1",'
                    ' "2021-10-18 16:20", "2021-10-18 16:20");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO settings "
                    f'VALUES ("{uuid4()}", "{uuid}", "test_key_2", "test_value_2",'
                    ' "2021-10-18 16:20", "2021-10-18 16:30");'
                )
            )
            connection.commit()
            last_sync_datetime = datetime(2021, 10, 18, 16, 25)

            # act
            settings = database_handler.get_settings(UUID(uuid), last_sync_datetime)

            # assert
            self.assertEqual(len(settings), 1)
            self.assertEqual(settings[0].key, "test_key_2")
            self.assertEqual(settings[0].value, "test_value_2")
            self.assertEqual(settings[0].created_at, "2021-10-18 16:20")
            self.assertEqual(settings[0].updated_at, "2021-10-18 16:30")

    def test_get_settings_only_from_authenticated_user(self):
        """test get_settings method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            database_handler = DatabaseHandler(connection)
            uuid = "3c8116631cf8495d8267ca18abcd1dac"
            connection.execute_sql(
                (
                    "INSERT INTO users "
                    f'VALUES ("{uuid}", "test_user",'
                    '"$2b$12$ipvsDZVBzSvEr/8qKljcPeRJ5bR3EgkRjzpMkOqgbkg2ugQLqrMza",'
                    f' "{datetime.now()}", "{datetime.now()}");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO settings "
                    f'VALUES ("{uuid4()}", "{uuid4()}", "test_key_1", "test_value_1",'
                    f'"{datetime.now()}", "{datetime.now()}");'
                )
            )
            connection.execute_sql(
                (
                    "INSERT INTO settings "
                    f'VALUES ("{uuid4()}", "{uuid}", "test_key_2", "test_value_2",'
                    f'"{datetime.now()}", "{datetime.now()}");'
                )
            )
            connection.commit()

            # act
            settings = database_handler.get_settings(UUID(uuid))

            # assert
            self.assertEqual(len(settings), 1)
            self.assertEqual(settings[0].key, "test_key_2")
            self.assertEqual(settings[0].value, "test_value_2")

    def test_get_user_no_user(self):
        """test get_user method"""
        # arrange
        database_handler = DatabaseHandler(":memory:")

        # act
        user = database_handler.get_user("test_user")

        # assert
        self.assertIsNone(user)

    def test_get_user_user(self):
        """test get_user method"""
        with SqliteDatabase(":memory:") as connection:
            # arrange
            database_handler = DatabaseHandler(connection)
            connection.execute_sql(
                (
                    "INSERT INTO users "
                    'VALUES ("51288003-49dc-4e34-82d4-51f84fd9e59f", "test_user",'
                    '"$2b$12$ipvsDZVBzSvEr/8qKljcPeRJ5bR3EgkRjzpMkOqgbkg2ugQLqrMza",'
                    '"2021-10-18 16:20", "2021-10-18 16:30");'
                )
            )
            connection.commit()

            # act
            user = database_handler.get_user("test_user")

            # assert
            self.assertIsNotNone(user)
            self.assertEqual(user.id, UUID("51288003-49dc-4e34-82d4-51f84fd9e59f"))
            self.assertEqual(user.username, "test_user")
            self.assertEqual(
                user.password_hash,
                "$2b$12$ipvsDZVBzSvEr/8qKljcPeRJ5bR3EgkRjzpMkOqgbkg2ugQLqrMza",
            )

    def test_save_user_none_error(self):
        """test save_user method"""
        # arrange
        database_handler = DatabaseHandler(":memory:")

        # act & assert
        with raises(ValueError):
            database_handler.save_user(None)

    def test_save_user_valid_data_user(self):
        """test save_user method"""
        # arrange
        with SqliteDatabase(":memory:") as connection:
            # arrange
            database_handler = DatabaseHandler(connection)
            user = User()
            user.username = "test_user"
            user.password_hash = (
                "$2b$12$ipvsDZVBzSvEr/8qKljcPeRJ5bR3EgkRjzpMkOqgbkg2ugQLqrMza"
            )

            # act
            database_handler.save_user(user)

            # assert
            cursor = connection.execute_sql("SELECT * FROM users;")
            row = cursor.fetchall()
            self.assertEqual(len(row), 1)
            user_database = row[0]
            self.assertEqual(UUID(user_database[0]), user.id)
            self.assertEqual(user_database[1], user.username)
            self.assertEqual(user_database[2], user.password_hash)


if __name__ == '__main__':
    unittest.main()
