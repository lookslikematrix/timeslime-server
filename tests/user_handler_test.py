"""user handler test"""
import unittest
from unittest.mock import Mock

from pytest import raises

from timeslime_server.handler import DatabaseHandler, UserHandler
from timeslime_server.model import User


class UserHandlerTests(unittest.TestCase):
    """class for testing the UserHandler"""

    def test_create_user_username_is_missing_error(self):
        """test create_user method"""
        # arrange
        user_handler = UserHandler(None)

        # act & assert
        with raises(ValueError):
            user_handler.create(None, "test_password")

    def test_create_user_username_is_empty_string_error(self):
        """test create_user method"""
        # arrange
        user_handler = UserHandler(None)

        # act & assert
        with raises(ValueError):
            user_handler.create("", "test_password")

    def test_create_user_username_is_only_whitespaces_string_error(self):
        """test create_user method"""
        # arrange
        user_handler = UserHandler(None)

        # act & assert
        with raises(ValueError):
            user_handler.create(" ", "test_password")

    def test_create_user_password_is_missing_error(self):
        """test create_user method"""
        # arrange
        user_handler = UserHandler(None)

        # act & assert
        with raises(ValueError):
            user_handler.create("test_user", None)

    def test_create_user_password_is_empty_string_error(self):
        """test create_user method"""
        # arrange
        user_handler = UserHandler(None)

        # act & assert
        with raises(ValueError):
            user_handler.create("test_user", "")

    def test_create_user_password_is_only_whitespaces_string_error(self):
        """test create_user method"""
        # arrange
        user_handler = UserHandler(None)

        # act & assert
        with raises(ValueError):
            user_handler.create("test_user", " ")

    def test_create_user_valid_data_success(self):
        """test create_user method"""
        # arrange
        database_handler = DatabaseHandler(":memory:")
        save_user_mock = Mock()
        database_handler.save_user = save_user_mock
        user_handler = UserHandler(database_handler)

        # act
        user_handler.create("test_user", "test_password")

        # assert
        save_user_mock.assert_called()
        called_user = save_user_mock.call_args_list[0][0][0]
        self.assertIsNotNone(called_user)
        self.assertEqual(called_user.username, "test_user")
        self.assertIsNotNone(called_user.id)
        self.assertIsNotNone(called_user.password_hash)

    def test_validate_user_no_user(self):
        """test validate_user method"""
        # arrange
        database_handler = DatabaseHandler(":memory:")
        user_handler = UserHandler(database_handler)

        # act
        response = user_handler.validate_user("test_user", "test_password")

        # assert
        self.assertFalse(response)

    def test_validate_user_wrong_password_false(self):
        """test validate_user method"""
        database_handler = DatabaseHandler(":memory:")
        user = User()
        user.username = "test_user"
        user.password_hash = (
            "$2b$12$ipvsDZVBzSvEr/8qKljcPeRJ5bR3EgkRjzpMkOqgbkg2ugQLqrMza"
        )
        database_handler.get_user = Mock(return_value=user)
        user_handler = UserHandler(database_handler)

        # act
        response = user_handler.validate_user("test_user", "test_password")

        # assert
        self.assertFalse(response)

    def test_validate_user_correct_password_true(self):
        """test validate_user method"""
        database_handler = DatabaseHandler(":memory:")
        user = User()
        user.username = "test_user"
        user.password_hash = (
            "$2b$12$Qpm8ZQkohM2YnmEoE5TWeuOxoMITBE47iTz0hUNKeKvzqTbSUt9Wa"
        )
        database_handler.get_user = Mock(return_value=user)
        user_handler = UserHandler(database_handler)

        # act
        response = user_handler.validate_user("test_user", "test_password")

        # assert
        self.assertTrue(response)

    def test_get_authenticated_user_no_user(self):
        """test get_authenticated_user method"""
        # arrange
        database_handler = DatabaseHandler(":memory:")
        user_handler = UserHandler(database_handler)

        # act
        user = user_handler.get_authenticated_user("test_user", "test_password")

        # assert
        self.assertIsNone(user)

    def test_get_authenticated_user_wrong_password_no_user(self):
        """test get_authenticated_user method"""
        database_handler = DatabaseHandler(":memory:")
        user = User()
        user.username = "test_user"
        user.password_hash = (
            "$2b$12$ipvsDZVBzSvEr/8qKljcPeRJ5bR3EgkRjzpMkOqgbkg2ugQLqrMza"
        )
        database_handler.get_user = Mock(return_value=user)
        user_handler = UserHandler(database_handler)

        # act
        user = user_handler.get_authenticated_user("test_user", "test_password")

        # assert
        self.assertIsNone(user)

    def test_get_authenticated_user_correct_password_user(self):
        """test get_authenticated_user method"""
        database_handler = DatabaseHandler(":memory:")
        user = User()
        user.username = "test_user"
        user.password_hash = (
            "$2b$12$Qpm8ZQkohM2YnmEoE5TWeuOxoMITBE47iTz0hUNKeKvzqTbSUt9Wa"
        )
        database_handler.get_user = Mock(return_value=user)
        user_handler = UserHandler(database_handler)

        # act
        user = user_handler.get_authenticated_user("test_user", "test_password")

        # assert
        self.assertIsNotNone(user)
        self.assertEqual(user.username, "test_user")
        self.assertEqual(
            user.password_hash,
            "$2b$12$Qpm8ZQkohM2YnmEoE5TWeuOxoMITBE47iTz0hUNKeKvzqTbSUt9Wa",
        )

if __name__ == "__main__":
    unittest.main()
