"""user handler test"""
import unittest
from datetime import datetime, timedelta, timezone

from pytest import raises

from timeslime_server.handler import DatetimeHandler


class DatetimeHandlerTest(unittest.TestCase):
    """class for testing the DatetimeHandler"""

    def test_string_to_datetime_incorrect_string_error(self):
        """test create_user method"""
        # arrange & act & assert
        with raises(ValueError):
            DatetimeHandler.string_to_datetime("test")

    def test_string_to_datetime_none_error(self):
        """test create_user method"""
        # arrange & act & assert
        with raises(TypeError):
            DatetimeHandler.string_to_datetime(None)

    def test_string_to_datetime_format_milliseconds_success(self):
        """test create_user method"""
        # arrange & act
        date = DatetimeHandler.string_to_datetime("2021-08-08 21:47:26.257399")

        # assert
        self.assertEqual(
            date,
            datetime(
                year=2021,
                month=8,
                day=8,
                hour=21,
                minute=47,
                second=26,
                microsecond=257399,
                tzinfo=timezone.utc,
            ),
        )

    def test_string_to_datetime_format_seconds_success(self):
        """test create_user method"""
        # arrange & act
        date = DatetimeHandler.string_to_datetime("2021-08-08 21:47:26")

        # assert
        self.assertEqual(
            date,
            datetime(
                year=2021,
                month=8,
                day=8,
                hour=21,
                minute=47,
                second=26,
                microsecond=0,
                tzinfo=timezone.utc,
            ),
        )

    def test_string_to_datetime_timezone_utc_success(self):
        """test create_user method"""
        # arrange & act
        date = DatetimeHandler.string_to_datetime("2021-08-08 21:47:26+00:00")

        # assert
        self.assertEqual(
            date,
            datetime(
                year=2021,
                month=8,
                day=8,
                hour=21,
                minute=47,
                second=26,
                microsecond=0,
                tzinfo=timezone.utc,
            ),
        )

    def test_string_to_datetime_timezone_utc_plus_two_success(self):
        """test create_user method"""
        # arrange & act
        date = DatetimeHandler.string_to_datetime("2021-08-08 21:47:26+02:00")

        # assert
        self.assertEqual(
            date,
            datetime(
                year=2021,
                month=8,
                day=8,
                hour=21,
                minute=47,
                second=26,
                microsecond=0,
                tzinfo=timezone(timedelta(seconds=7200)),
            ),
        )

    def test_string_to_datetime_from_javascript(self):
        """test create_user method"""
        # arrange & act
        date = DatetimeHandler.string_to_datetime("2021-08-08T21:47:26+00:00")

        # assert
        self.assertEqual(
            date,
            datetime(
                year=2021,
                month=8,
                day=8,
                hour=21,
                minute=47,
                second=26,
                microsecond=0,
                tzinfo=timezone.utc,
            ),
        )


if __name__ == "__main__":
    unittest.main()
