"""timespan serializer tests"""
import datetime
import unittest
import uuid

from pytest import raises

from timeslime_server.model import Timespan
from timeslime_server.serializer import TimespanSerializer


class TimeSpanSerializerTests(unittest.TestCase):
    """test class for the TimeSpanSerializer"""
    def test_deserialize_timespan_all_is_defined_success(self):
        """test deserialize method"""
        # arrange
        json_string = (
            "{"
            '"id": "51288003-49dc-4e34-82d4-51f84fd9e59d", '
            '"start_time": "2021-08-08 21:47:20.833440", '
            '"stop_time": "2021-08-08 21:47:26.257399"'
            "}"
        )
        timespan_serializer = TimespanSerializer()

        # act
        timespan = timespan_serializer.deserialize(json_string)

        # assert
        self.assertEqual(type(timespan.id), uuid.UUID)
        self.assertEqual(str(timespan.id), '51288003-49dc-4e34-82d4-51f84fd9e59d')
        self.assertEqual(type(timespan.start_time), datetime.datetime)
        self.assertEqual(str(timespan.start_time), "2021-08-08 21:47:20.833440+00:00")
        self.assertEqual(type(timespan.stop_time), datetime.datetime)
        self.assertEqual(str(timespan.stop_time), "2021-08-08 21:47:26.257399+00:00")

    def test_deserialize_timespan_with_created_and_updated_at(self):
        """test deserialize method"""
        # arrange
        json_string = {
            "id": "51288003-49dc-4e34-82d4-51f84fd9e59d",
            "start_time": "2021-08-08 21:47:20.833440",
            "stop_time": "2021-08-08 21:47:26.257399",
            "created_at": "2021-08-08 21:47:20.833440",
            "updated_at": "2021-08-08 21:47:20.833440",
        }
        timespan_serializer = TimespanSerializer()

        # act
        timespan = timespan_serializer.deserialize(json_string)

        # assert
        self.assertEqual(type(timespan.id), uuid.UUID)
        self.assertEqual(str(timespan.id), "51288003-49dc-4e34-82d4-51f84fd9e59d")
        self.assertEqual(type(timespan.start_time), datetime.datetime)
        self.assertEqual(str(timespan.start_time), "2021-08-08 21:47:20.833440+00:00")
        self.assertEqual(type(timespan.stop_time), datetime.datetime)
        self.assertEqual(str(timespan.stop_time), "2021-08-08 21:47:26.257399+00:00")
        self.assertEqual(type(timespan.created_at), datetime.datetime)
        self.assertEqual(str(timespan.created_at), "2021-08-08 21:47:20.833440+00:00")
        self.assertEqual(type(timespan.updated_at), datetime.datetime)
        self.assertEqual(str(timespan.updated_at), "2021-08-08 21:47:20.833440+00:00")

    def test_deserialize_timespan_with_different_date_format(self):
        """test deserialize method"""
        # arrange
        json_string = {
            "id": "51288003-49dc-4e34-82d4-51f84fd9e59d",
            "start_time": "2021-08-08 21:47:20",
            "stop_time": "2021-08-08 21:47:26",
            "created_at": "2021-08-08 21:47:20",
            "updated_at": "2021-08-08 21:47:20",
        }
        timespan_serializer = TimespanSerializer()

        # act
        timespan = timespan_serializer.deserialize(json_string)

        # assert
        self.assertEqual(type(timespan.id), uuid.UUID)
        self.assertEqual(str(timespan.id), "51288003-49dc-4e34-82d4-51f84fd9e59d")
        self.assertEqual(type(timespan.start_time), datetime.datetime)
        self.assertEqual(str(timespan.start_time), "2021-08-08 21:47:20+00:00")
        self.assertEqual(type(timespan.stop_time), datetime.datetime)
        self.assertEqual(str(timespan.stop_time), "2021-08-08 21:47:26+00:00")
        self.assertEqual(type(timespan.created_at), datetime.datetime)
        self.assertEqual(str(timespan.created_at), "2021-08-08 21:47:20+00:00")
        self.assertEqual(type(timespan.updated_at), datetime.datetime)
        self.assertEqual(str(timespan.updated_at), "2021-08-08 21:47:20+00:00")

    def test_deserialize_timespan_with_date_none_string(self):
        """test deserialize method"""
        # arrange
        json_string = {
            "id": "51288003-49dc-4e34-82d4-51f84fd9e59d",
            "start_time": "2021-08-08 21:47:20.833440",
            "stop_time": "2021-08-08 21:47:26.257399",
            "created_at": "None",
            "updated_at": "None",
        }
        timespan_serializer = TimespanSerializer()

        # act
        timespan = timespan_serializer.deserialize(json_string)

        # assert
        self.assertEqual(type(timespan.id), uuid.UUID)
        self.assertEqual(str(timespan.id), "51288003-49dc-4e34-82d4-51f84fd9e59d")
        self.assertEqual(type(timespan.start_time), datetime.datetime)
        self.assertEqual(str(timespan.start_time), "2021-08-08 21:47:20.833440+00:00")
        self.assertEqual(type(timespan.stop_time), datetime.datetime)
        self.assertEqual(str(timespan.stop_time), "2021-08-08 21:47:26.257399+00:00")

    def test_deserialize_timespan_with_date_none(self):
        """test deserialize method"""
        # arrange
        json_string = {
            "id": "51288003-49dc-4e34-82d4-51f84fd9e59d",
            "start_time": "2021-08-08 21:47:20.833440",
            "stop_time": "2021-08-08 21:47:26.257399",
            "created_at": None,
            "updated_at": None,
        }
        timespan_serializer = TimespanSerializer()

        # act
        timespan = timespan_serializer.deserialize(json_string)

        # assert
        self.assertEqual(type(timespan.id), uuid.UUID)
        self.assertEqual(str(timespan.id), "51288003-49dc-4e34-82d4-51f84fd9e59d")
        self.assertEqual(type(timespan.start_time), datetime.datetime)
        self.assertEqual(str(timespan.start_time), "2021-08-08 21:47:20.833440+00:00")
        self.assertEqual(type(timespan.stop_time), datetime.datetime)
        self.assertEqual(str(timespan.stop_time), "2021-08-08 21:47:26.257399+00:00")

    def test_deserialize_timespan_wrong_id_error(self):
        """test deserialize method"""
        # arrange
        json_string = '{"id": "test_id", "start_time": "2021-08-08 21:47:20.833440", "stop_time": "2021-08-08 21:47:26.257399"}'
        timespan_serializer = TimespanSerializer()

        # act & assert
        with raises(ValueError):
            timespan_serializer.deserialize(json_string)

    def test_deserialize_timespan_missing_id_success(self):
        """test deserialize method"""
        # arrange
        json_string = '{"start_time": "2021-08-08 21:47:20.833440", "stop_time": "2021-08-08 21:47:26.257399"}'
        timespan_serializer = TimespanSerializer()

        # act
        timespan = timespan_serializer.deserialize(json_string)

        # assert
        self.assertEqual(type(timespan.id), uuid.UUID)
        self.assertIsNotNone(timespan.id)
        self.assertEqual(type(timespan.start_time), datetime.datetime)
        self.assertEqual(str(timespan.start_time), "2021-08-08 21:47:20.833440+00:00")
        self.assertEqual(type(timespan.stop_time), datetime.datetime)
        self.assertEqual(str(timespan.stop_time), "2021-08-08 21:47:26.257399+00:00")

    def test_deserialize_timespan_missing_stop_time_success(self):
        """test deserialize method"""
        # arrange
        json_string = '{"start_time": "2021-08-08 21:47:20.833440"}'
        timespan_serializer = TimespanSerializer()

        # act
        timespan = timespan_serializer.deserialize(json_string)

        # assert
        self.assertEqual(type(timespan.id), uuid.UUID)
        self.assertIsNotNone(timespan.id)
        self.assertEqual(type(timespan.start_time), datetime.datetime)
        self.assertEqual(str(timespan.start_time), "2021-08-08 21:47:20.833440+00:00")
        self.assertIsNone(timespan.stop_time)

    def test_deserialize_timespan_missing_start_time_error(self):
        """test deserialize method"""
        # arrange
        json_string = '{"stop_time": "2021-08-08 21:47:26.257399"}'
        timespan_serializer = TimespanSerializer()

        # act & assert
        with raises(KeyError):
            timespan_serializer.deserialize(json_string)

    def test_deserialize_timespan_input_is_dict_success(self):
        """test deserialize method"""
        # arrange
        json_string = {"id": "51288003-49dc-4e34-82d4-51f84fd9e59d", "start_time": "2021-08-08 21:47:20.833440", "stop_time": "2021-08-08 21:47:26.257399"}
        timespan_serializer = TimespanSerializer()

        # act
        timespan = timespan_serializer.deserialize(json_string)

        # assert
        self.assertEqual(type(timespan.id), uuid.UUID)
        self.assertEqual(str(timespan.id), '51288003-49dc-4e34-82d4-51f84fd9e59d')
        self.assertEqual(type(timespan.start_time), datetime.datetime)
        self.assertEqual(str(timespan.start_time), "2021-08-08 21:47:20.833440+00:00")
        self.assertEqual(type(timespan.stop_time), datetime.datetime)
        self.assertEqual(str(timespan.stop_time), "2021-08-08 21:47:26.257399+00:00")

    def test_deserialize_timespan_stop_time_is_none_success(self):
        """test deserialize method"""
        # arrange
        json_string = {"id": "51288003-49dc-4e34-82d4-51f84fd9e59d", "start_time": "2021-08-08 21:47:20.833440", "stop_time": "None"}
        timespan_serializer = TimespanSerializer()

        # act
        timespan = timespan_serializer.deserialize(json_string)

        # assert
        self.assertEqual(type(timespan.id), uuid.UUID)
        self.assertEqual(str(timespan.id), '51288003-49dc-4e34-82d4-51f84fd9e59d')
        self.assertEqual(type(timespan.start_time), datetime.datetime)
        self.assertEqual(str(timespan.start_time), "2021-08-08 21:47:20.833440+00:00")
        self.assertIsNone(timespan.stop_time)

    def test_deserialize_timespan_stop_time_is_really_none_success(self):
        """test deserialize method"""
        # arrange
        json_string = {"id": "51288003-49dc-4e34-82d4-51f84fd9e59d", "start_time": "2021-08-08 21:47:20.833440", "stop_time": None}
        timespan_serializer = TimespanSerializer()

        # act
        timespan = timespan_serializer.deserialize(json_string)

        # assert
        self.assertEqual(type(timespan.id), uuid.UUID)
        self.assertEqual(str(timespan.id), '51288003-49dc-4e34-82d4-51f84fd9e59d')
        self.assertEqual(type(timespan.start_time), datetime.datetime)
        self.assertEqual(str(timespan.start_time), "2021-08-08 21:47:20.833440+00:00")
        self.assertIsNone(timespan.stop_time)

    def test_deserialize_timespan_start_time_is_none_error(self):
        """test deserialize method"""
        # arrange
        json_string = {
            "id": "51288003-49dc-4e34-82d4-51f84fd9e59d",
            "start_time": "None",
        }
        timespan_serializer = TimespanSerializer()

        # act & assert
        with raises(KeyError):
            timespan_serializer.deserialize(json_string)

    def test_deserialize_timespan_start_time_is_really_none_error(self):
        """test deserialize method"""
        # arrange
        json_string = {
            "id": "51288003-49dc-4e34-82d4-51f84fd9e59d",
            "start_time": None,
        }
        timespan_serializer = TimespanSerializer()

        # act & assert
        with raises(KeyError):
            timespan_serializer.deserialize(json_string)

    def test_serialize_timespan_everything_is_none(self):
        """test serialize method"""
        # arrange
        timespan = Timespan()
        timespan_serializer = TimespanSerializer()

        # act
        json_string = timespan_serializer.serialize(timespan)

        # assert
        json_response = {
            "id": str(timespan.id),
            "start_time": None,
            "stop_time": None,
        }
        self.assertEqual(json_string["id"], json_response["id"])
        self.assertEqual(json_string["start_time"], json_response["start_time"])
        self.assertEqual(json_string["stop_time"], json_response["stop_time"])
        self.assertIsNotNone(json_string["created_at"])
        self.assertIsNotNone(json_string["updated_at"])

    def test_serialize_timespan(self):
        """test serialize method"""
        # arrange
        timespan = Timespan()
        timespan.start_time = datetime.datetime.now()
        timespan.stop_time = datetime.datetime.now()
        timespan_serializer = TimespanSerializer()

        # act
        json_string = timespan_serializer.serialize(timespan)

        # assert
        json_response = {
            "id": str(timespan.id),
            "start_time": str(timespan.start_time),
            "stop_time": str(timespan.stop_time),
        }
        self.assertEqual(json_string["id"], json_response["id"])
        self.assertEqual(json_string["start_time"], json_response["start_time"])
        self.assertEqual(json_string["stop_time"], json_response["stop_time"])
        self.assertIsNotNone(json_string["created_at"])
        self.assertIsNotNone(json_string["updated_at"])

    def test_serialize_none_timespan(self):
        """test serialize method"""
        # arrange
        timespan = Timespan()
        timespan.start_time = None
        timespan.stop_time = None
        timespan_serializer = TimespanSerializer()

        # act
        json_string = timespan_serializer.serialize(timespan)

        # assert
        json_response = {
            "id": str(timespan.id),
            "start_time": None,
            "stop_time": None,
        }
        self.assertEqual(json_string["id"], json_response["id"])
        self.assertEqual(json_string["start_time"], json_response["start_time"])
        self.assertEqual(json_string["stop_time"], json_response["stop_time"])
        self.assertIsNotNone(json_string["created_at"])
        self.assertIsNotNone(json_string["updated_at"])

    def test_serialize_list_empty_timespans(self):
        """test serialize_list method"""
        # arrange
        timespan_serializer = TimespanSerializer()

        # act
        json_string = timespan_serializer.serialize_list([])

        # assert
        json_response = []
        self.assertEqual(json_string, json_response)

    def test_serialize_list_correct_timespans(self):
        """test serialize_list method"""
        # arrange
        timespan = Timespan()
        timespan.start_time = datetime.datetime.now()
        timespan.stop_time = datetime.datetime.now()
        timespan_serializer = TimespanSerializer()

        # act
        json_string = timespan_serializer.serialize_list([timespan])

        # assert
        json_response = [
            {
                "id": str(timespan.id),
                "start_time": str(timespan.start_time),
                "stop_time": str(timespan.stop_time),
            }
        ]
        self.assertEqual(len(json_string), 1)
        self.assertEqual(json_string[0]["id"], json_response[0]["id"])
        self.assertEqual(json_string[0]["start_time"], json_response[0]["start_time"])
        self.assertEqual(json_string[0]["stop_time"], json_response[0]["stop_time"])
        self.assertIsNotNone(json_string[0]["created_at"])
        self.assertIsNotNone(json_string[0]["updated_at"])

    def test_serialize_list_wrong_timespans(self):
        """test serialize_list method"""
        # arrange
        timespan = Timespan()
        timespan_serializer = TimespanSerializer()

        # act
        json_string = timespan_serializer.serialize_list([timespan])

        # assert
        json_response = [
            {"id": str(timespan.id), "start_time": None, "stop_time": None}
        ]
        self.assertEqual(len(json_string), 1)
        self.assertEqual(json_string[0]["id"], json_response[0]["id"])
        self.assertEqual(json_string[0]["start_time"], json_response[0]["start_time"])
        self.assertEqual(json_string[0]["stop_time"], json_response[0]["stop_time"])
        self.assertIsNotNone(json_string[0]["created_at"])
        self.assertIsNotNone(json_string[0]["updated_at"])

if __name__ == '__main__':
    unittest.main()
