"""collection of handler classes"""
from datetime import datetime, timezone
from typing import Union
from uuid import UUID

from bcrypt import checkpw, gensalt, hashpw
from peewee import SqliteDatabase

from timeslime_server.model import Setting, Timespan, User


class DatabaseHandler():
    """handler to read and write to the database"""

    def __init__(self, database_connection):
        if isinstance(database_connection, str):
            self.connection = SqliteDatabase(database_connection)
        else:
            self.connection = database_connection
        models = [Setting, Timespan, User]
        self.connection.bind(models)
        self.connection.create_tables(models)

    def __del__(self):
        self.connection.close()

    def save_timespan(self, timespan: Timespan):
        """save timespan
        :param timespan: defines timespan
        """
        if not isinstance(timespan, Timespan):
            return

        self.save_timespans([timespan])

    def save_timespans(self, timespans: list):
        """save timespans
        :param timespans: defines an array of timespans"""
        if not isinstance(timespans, list):
            return

        for timespan in timespans:
            if timespan.start_time is None:
                return

            if timespan.updated_at.tzinfo is None:
                timespan.updated_at = timespan.updated_at.replace(tzinfo=timezone.utc)

            old_timespan = Timespan.get_or_none(Timespan.id == timespan.id)
            if old_timespan:
                if old_timespan.updated_at.tzinfo is None:
                    old_timespan.updated_at = old_timespan.updated_at.replace(
                        tzinfo=timezone.utc
                    )

                if old_timespan.updated_at >= timespan.updated_at:
                    continue
                query = Timespan.delete().where(Timespan.id == timespan.id)
                query.execute()
                timespan.created_at = old_timespan.created_at
            timespan.updated_at = datetime.now(timezone.utc)
            timespan.save(force_insert=True)

    def get_timespans(
        self,
        user_id: UUID,
        updated_at: datetime = None,
        from_date: datetime = None,
        to_date: datetime = None,
    ):
        """get timespans"""
        if updated_at is None and from_date is None and to_date is None:
            return Timespan.select().join(User).where(User.id == user_id)

        if updated_at is not None and from_date is None and to_date is None:
            return (
                Timespan.select()
                .join(User)
                .where((User.id == user_id) & (Timespan.updated_at > updated_at))
            )

        if updated_at is not None and from_date is not None and to_date is None:
            return (
                Timespan.select()
                .join(User)
                .where(
                    (User.id == user_id)
                    & (Timespan.updated_at > updated_at)
                    & (Timespan.start_time >= from_date)
                )
            )

        if updated_at is not None and from_date is None and to_date is not None:
            return (
                Timespan.select()
                .join(User)
                .where(
                    (User.id == user_id)
                    & (Timespan.updated_at > updated_at)
                    & (Timespan.stop_time <= to_date)
                )
            )

        if updated_at is None and from_date is not None and to_date is None:
            return (
                Timespan.select()
                .join(User)
                .where((User.id == user_id) & (Timespan.start_time > from_date))
            )

        if updated_at is None and from_date is not None and to_date is not None:
            return (
                Timespan.select()
                .join(User)
                .where(
                    (User.id == user_id)
                    & (Timespan.start_time >= from_date)
                    & (Timespan.stop_time <= to_date)
                )
            )

        if updated_at is None and from_date is None and to_date is not None:
            return (
                Timespan.select()
                .join(User)
                .where((User.id == user_id) & (Timespan.stop_time <= to_date))
            )

        return (
            Timespan.select()
            .join(User)
            .where(
                (User.id == user_id)
                & (Timespan.updated_at > updated_at)
                & (Timespan.start_time >= from_date)
                & (Timespan.stop_time <= to_date)
            )
        )

    def save_setting(self, setting: Setting):
        """save setting
        :param setting: defines setting
        """
        if not isinstance(setting, Setting):
            return

        self.save_settings([setting])

    def save_settings(self, settings: list):
        """save settings
        :param settings: defines an array of settings"""
        if not isinstance(settings, list):
            return

        for setting in settings:
            if setting.key is None:
                return

            if setting.updated_at.tzinfo is None:
                setting.updated_at = setting.updated_at.replace(tzinfo=timezone.utc)

            old_setting = Setting.get_or_none(Setting.key == setting.key)
            if old_setting:
                if old_setting.updated_at.tzinfo is None:
                    old_setting.updated_at = old_setting.updated_at.replace(
                        tzinfo=timezone.utc
                    )

                setting.id = old_setting.id
                if old_setting.updated_at >= setting.updated_at:
                    continue
                query = Setting.delete().where(Setting.key == setting.key)
                query.execute()
                setting.created_at = old_setting.created_at
            setting.updated_at = datetime.now(timezone.utc)
            setting.save(force_insert=True)

    def get_settings(self, user_id: UUID, filter_datetime: datetime = None):
        """get settings"""
        if filter_datetime is None:
            return Setting.select().join(User).where(User.id == user_id)

        return (
            Setting.select()
            .join(User)
            .where((User.id == user_id) & (Setting.updated_at > filter_datetime))
        )

    def save_user(self, user: User) -> None:
        """save user
        :param user: defines user"""
        if not isinstance(user, User):
            raise ValueError

        user.updated_at = datetime.now(timezone.utc)
        user.save(force_insert=True)

    def get_user(self, username: str) -> User:
        """get user
        :param username: defines username"""
        return User.get_or_none(User.username == username)


class UserHandler:
    """handler for users"""

    def __init__(self, database_handler: DatabaseHandler) -> None:
        self.database_handler = database_handler

    def create(self, username: str, password: str):
        """create a user"""
        if username is None or not username or username.isspace():
            raise ValueError

        if password is None or not password or password.isspace():
            raise ValueError

        user = User()
        user.username = username
        user.password_hash = hashpw(bytes(password, "utf-8"), gensalt())
        self.database_handler.save_user(user)

    def validate_user(self, username: str, password: str) -> bool:
        """check if username and password are valid
        :param username: defines username
        :param password: defines password"""
        user = self.get_authenticated_user(username, password)
        if user is None:
            return False
        return True

    def get_authenticated_user(self, username: str, password: str) -> Union[User, None]:
        """get authenticated user
        :param username: defines username
        :param password: defines password"""
        user = self.database_handler.get_user(username)
        if user is None:
            return None
        if checkpw(bytes(password, "utf-8"), bytes(user.password_hash, "utf-8")):
            return user
        return None


class DatetimeHandler:
    """handler for datetime"""

    @classmethod
    def string_to_datetime(cls, date_string: str) -> datetime:
        """convert a string into a datetime
        :param date_string: define date which should be converted"""
        if not date_string:
            raise TypeError

        date_string = date_string.replace("Z", "+00:00")
        date = datetime.fromisoformat(date_string)
        if date.tzinfo is None:
            date = date.replace(tzinfo=timezone.utc)
        return date
