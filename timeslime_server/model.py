"""collection of data models"""
from datetime import datetime, timezone
from uuid import uuid4

from peewee import DateTimeField, ForeignKeyField, Model, TextField, UUIDField

DateTimeField.formats = [
    "%Y-%m-%d %H:%M:%S.%f%z",
    "%Y-%m-%d %H:%M:%S.%f",
    "%Y-%m-%d %H:%M:%S%z",
    "%Y-%m-%d %H:%M:%S",
    "%Y-%m-%d",
]

class User(Model):
    """user model"""

    id = UUIDField(primary_key=True, default=uuid4)
    username = TextField(index=True, null=False, unique=True)
    password_hash = TextField(null=False)
    created_at = DateTimeField(
        default=lambda: datetime.now(tz=timezone.utc), null=False
    )
    updated_at = DateTimeField(
        default=lambda: datetime.now(tz=timezone.utc), null=False
    )

    class Meta:
        """defines meta information"""

        table_name = "users"


class Setting(Model):
    """setting model"""

    id = UUIDField(primary_key=True, default=uuid4)
    user = ForeignKeyField(User, backref="settings", index=True, null=False)
    key = TextField(index=True, null=True)
    value = TextField(null=True)
    created_at = DateTimeField(
        default=lambda: datetime.now(tz=timezone.utc), null=False
    )
    updated_at = DateTimeField(
        default=lambda: datetime.now(tz=timezone.utc), null=False
    )

    class Meta:
        """defines meta information"""

        table_name = "settings"


class Timespan(Model):
    """timespan model"""

    id = UUIDField(primary_key=True, default=uuid4)
    user = ForeignKeyField(User, backref="timespans", index=True, null=False)
    start_time = DateTimeField(null=True)
    stop_time = DateTimeField(null=True)
    created_at = DateTimeField(
        default=lambda: datetime.now(tz=timezone.utc), null=False
    )
    updated_at = DateTimeField(
        default=lambda: datetime.now(tz=timezone.utc), null=False
    )

    class Meta:
        """defines meta information"""

        table_name = "timespans"
