"""database connection"""
from flask import current_app, g
from peewee import SqliteDatabase


def get_db():
    """get database connection"""
    if "db" not in g:
        database_connection = SqliteDatabase(current_app.config["DATABASE"])
        g.setdefault("db", database_connection)

    return g.db


def close_db(_=None):
    """close database connection"""
    database_connection = g.pop("db", None)

    if database_connection is not None:
        database_connection.close()

def init_app(app):
    """initialize application"""
    app.teardown_appcontext(close_db)
