"""collection of serializer classes"""
from datetime import datetime
from json import loads
from uuid import UUID

from timeslime_server.handler import DatetimeHandler
from timeslime_server.model import Setting, Timespan


class TimespanSerializer:
    """serializer for a timespan object"""

    @classmethod
    def deserialize(cls, json_string: str) -> Timespan:
        """deserialize a json string into a Timespan object
        :param json_string: defines json string"""
        if isinstance(json_string, str):
            timespan_object = loads(json_string)
        else:
            timespan_object = json_string

        timespan = Timespan()

        if 'id' in timespan_object:
            timespan.id = UUID(timespan_object['id'])

        if (
            "created_at" in timespan_object
            and timespan_object["created_at"] != "None"
            and timespan_object["created_at"] is not None
        ):
            timespan.created_at = DatetimeHandler.string_to_datetime(
                timespan_object["created_at"]
            )

        if (
            "updated_at" in timespan_object
            and timespan_object["updated_at"] != "None"
            and timespan_object["updated_at"] is not None
        ):
            timespan.updated_at = DatetimeHandler.string_to_datetime(
                timespan_object["updated_at"]
            )


        if (timespan_object["start_time"] == "None"
            or timespan_object["start_time"] is None
        ):
            raise KeyError

        timespan.start_time = DatetimeHandler.string_to_datetime(
            timespan_object["start_time"]
        )

        if "stop_time" in timespan_object and not (
            timespan_object["stop_time"] == "None"
            or timespan_object["stop_time"] is None
        ):
            timespan.stop_time = DatetimeHandler.string_to_datetime(
                timespan_object["stop_time"]
            )

        return timespan

    @classmethod
    def serialize(cls, timespan: Timespan):
        """serialize a Timespan object into a string
        :param timespan: defines Timespan object"""

        start_time = None
        if timespan.start_time:
            start_time = str(timespan.start_time)

        stop_time = None
        if timespan.stop_time:
            stop_time = str(timespan.stop_time)

        created_at = None
        if timespan.created_at:
            created_at = str(timespan.created_at)

        updated_at = None
        if timespan.updated_at:
            updated_at = str(timespan.updated_at)

        return {
            "id": str(timespan.id),
            "start_time": start_time,
            "stop_time": stop_time,
            "created_at": created_at,
            "updated_at": updated_at,
        }

    def serialize_list(self, timespans: list):
        """serialize Timespan objects into a string
        :param timespans: defines a list of Timespan objects"""
        timespans_json = []
        for timespan in timespans:
            timespans_json.append(self.serialize(timespan))

        return timespans_json


class SettingSerializer:
    """serializer for a setting object"""

    @classmethod
    def deserialize(cls, json_string: str) -> Setting:
        """deserialize a json string into a Setting object
        :param json_string: defines json string"""
        if isinstance(json_string, str):
            setting_object = loads(json_string)
        else:
            setting_object = json_string

        setting = Setting()

        if "id" in setting_object:
            setting.id = UUID(setting_object["id"])

        if (
            "created_at" in setting_object
            and setting_object["created_at"] != "None"
            and setting_object["created_at"] is not None
        ):
            setting.created_at = DatetimeHandler.string_to_datetime(
                setting_object["created_at"]
            )

        if (
            "updated_at" in setting_object
            and setting_object["updated_at"] != "None"
            and setting_object["updated_at"] is not None
        ):
            setting.updated_at = DatetimeHandler.string_to_datetime(
                setting_object["updated_at"]
            )

        if setting_object["key"] == "None":
            raise KeyError

        setting.key = setting_object["key"]

        if "value" in setting_object:
            if setting_object["value"] == "None":
                return setting
            setting.value = setting_object["value"]
        return setting

    @classmethod
    def serialize(cls, setting: Setting):
        """serialize a Setting object into a string
        :param setting: defines Setting object"""
        created_at = None
        if setting.created_at:
            created_at = str(setting.created_at)

        updated_at = None
        if setting.updated_at:
            updated_at = str(setting.updated_at)

        return {
            "id": str(setting.id),
            "key": setting.key,
            "value": setting.value,
            "created_at": created_at,
            "updated_at": updated_at,
        }

    def serialize_list(self, settings: list):
        """serialize Setting objects into a string
        :param settings: defines a list of Setting objects"""
        settings_json = []
        for setting in settings:
            settings_json.append(self.serialize(setting))

        return settings_json
