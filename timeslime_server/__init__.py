"""timeslime-server - It's a server for your timeslime data."""
from os import mkdir
from os.path import dirname, exists, join
from secrets import token_hex

import click
from flask import Flask
from flask.cli import with_appcontext

from timeslime_server.controller import (
    me_blueprint,
    settings_blueprint,
    timespans_blueprint,
)
from timeslime_server.db import get_db, init_app
from timeslime_server.handler import DatabaseHandler, UserHandler


def create_app(test_config=None):
    """create application"""
    app = Flask(__name__, instance_relative_config=True)
    app.secret_key = bytes(token_hex(), "utf-8")

    database_path = join('data', 'data.db')
    if not exists(database_path):
        directory = dirname(database_path)
        if directory != '' and not exists(directory):
            mkdir(directory)

    app.config.from_mapping(
        DATABASE=database_path,
        SESSION_COOKIE_SAMESITE="Strict",
    )

    if test_config is not None:
        app.config.from_mapping(test_config)

    with app.app_context():
        database_handler = DatabaseHandler(get_db())
        if test_config is not None:
            user_handler = UserHandler(database_handler)
            user_handler.create("test_user", "test_password")

    init_app(app)

    app.register_blueprint(me_blueprint)
    app.register_blueprint(settings_blueprint)
    app.register_blueprint(timespans_blueprint)

    app.cli.add_command(create_user)

    return app


@click.command("create-user")
@click.option("--username", required=True, help="defines username")
@click.option("--password", required=True, help="defines password")
@with_appcontext
def create_user(username, password):
    """create a user with the user handler"""
    database_handler = DatabaseHandler(get_db())
    user_handler = UserHandler(database_handler)
    user_handler.create(username, password)
