"""collection of controller"""
from datetime import datetime

from flask import Blueprint, jsonify, request, session

from timeslime_server.db import get_db
from timeslime_server.handler import DatabaseHandler, DatetimeHandler, UserHandler
from timeslime_server.serializer import SettingSerializer, TimespanSerializer

me_blueprint = Blueprint("me", __name__, url_prefix="/api/v1/me")
settings_blueprint = Blueprint("settings", __name__, url_prefix="/api/v1/settings")
timespans_blueprint = Blueprint("timespans", __name__, url_prefix="/api/v1/timespans")


def requires_authentication(func):
    """decorator to requires authentication"""

    def check_authentication(*args, **kwargs):
        """check authentication"""
        if request.authorization is None and "user_id" not in session:
            return "Please provide user credentials with your request", 403

        if "user_id" in session:
            return func(*args, **kwargs)

        database_handler = DatabaseHandler(get_db())
        user_handler = UserHandler(database_handler)

        user = user_handler.get_authenticated_user(
            request.authorization.username, request.authorization.password
        )
        if user is None:
            return "User is not authenticated", 403
        session["user_id"] = user.id
        session["user_name"] = user.username
        return func(*args, **kwargs)

    check_authentication.__name__ = func.__name__
    return check_authentication


@timespans_blueprint.route("", methods=["POST"])
@requires_authentication
def create_timespan():
    """ "create timespan route"""
    if not request.is_json:
        return "Content-Type must be application/json", 400
    timespan_serializer = TimespanSerializer()
    database_handler = DatabaseHandler(get_db())
    try:
        if not isinstance(request.json, list):
            timespan = timespan_serializer.deserialize(request.json)
            timespan.user = session["user_id"]
            database_handler.save_timespan(timespan)
            return jsonify(timespan_serializer.serialize(timespan))

        timespans = []
        for json_timespan in request.json:
            try:
                timespan = timespan_serializer.deserialize(json_timespan)
                timespan.user = session["user_id"]
                timespans.append(timespan)
            except KeyError:
                pass
        database_handler.save_timespans(timespans)
        return jsonify(timespan_serializer.serialize_list(timespans))

    except (KeyError, ValueError):
        return "Could not serialize object", 400


@timespans_blueprint.route("", methods=["GET"])
@requires_authentication
def get_timespans():
    """get timespan route"""
    [updated_at, from_date, to_date] = __get_arg_timespans_query()
    request_time = datetime.utcnow()
    timespan_serializer = TimespanSerializer()
    database_handler = DatabaseHandler(get_db())
    timespans = database_handler.get_timespans(
        session["user_id"], updated_at, from_date, to_date
    )
    timespans_json = []
    for timespan in timespans:
        timespans_json.append(timespan_serializer.serialize(timespan))

    return jsonify({"data": timespans_json, "request_time": str(request_time)})


def __get_arg_timespans_query():
    """get from body or parameter the from value"""
    updated_at = None
    from_date = None
    to_date = None
    if len(request.data) > 0 and "filter_datetime" in request.json:
        try:
            updated_at = DatetimeHandler.string_to_datetime(
                request.json["filter_datetime"]
            )
        except ValueError:
            pass

        return [updated_at, None, None]

    if len(request.args) > 0 and "updated_at" in request.args:
        try:
            updated_at = DatetimeHandler.string_to_datetime(
                request.args.get("updated_at")
            )
        except ValueError:
            pass

    if len(request.args) > 0 and "from" in request.args:
        try:
            from_date = DatetimeHandler.string_to_datetime(request.args.get("from"))
        except ValueError:
            pass

    if len(request.args) > 0 and "to" in request.args:
        try:
            to_date = DatetimeHandler.string_to_datetime(request.args.get("to"))
        except ValueError:
            pass

    return [updated_at, from_date, to_date]

@settings_blueprint.route("", methods=["POST"])
@requires_authentication
def create_setting():
    """create setting route"""
    if not request.is_json:
        return "Content-Type must be application/json", 400
    setting_serializer = SettingSerializer()
    database_handler = DatabaseHandler(get_db())
    try:
        if not isinstance(request.json, list):
            setting = setting_serializer.deserialize(request.json)
            setting.user = session["user_id"]
            database_handler.save_setting(setting)
            return jsonify(setting_serializer.serialize(setting))

        settings = []
        for json_setting in request.json:
            try:
                setting = setting_serializer.deserialize(json_setting)
                setting.user = session["user_id"]
                settings.append(setting)
            except KeyError:
                pass
        database_handler.save_settings(settings)
        return jsonify(setting_serializer.serialize_list(settings))
    except (KeyError, ValueError):
        return "Could not serialize object", 400


@settings_blueprint.route("", methods=["GET"])
@requires_authentication
def get_settings():
    """get setting route"""
    filter_datetime = None
    if len(request.data) > 0 and "filter_datetime" in request.json:
        try:
            filter_datetime = DatetimeHandler.string_to_datetime(
                request.json["filter_datetime"]
            )
        except ValueError:
            pass
    request_time = datetime.utcnow()
    setting_serializer = SettingSerializer()
    database_handler = DatabaseHandler(get_db())
    settings = database_handler.get_settings(session["user_id"], filter_datetime)
    settings_json = []
    for setting in settings:
        settings_json.append(setting_serializer.serialize(setting))

    return jsonify({"data": settings_json, "request_time": str(request_time)})


@me_blueprint.route("", methods=["GET"])
@requires_authentication
def get_me():
    """get me route"""
    return jsonify({"id": session["user_id"], "username": session["user_name"]})
