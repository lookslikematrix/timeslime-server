# timeslime-server

## 1.4

* add `to` GET parameter to `/api/v1/timespans` route

* add `/api/v1/me` route for getting information about the current logged in user

* add filter for `start_time`

* bugfix:

    * use `DatetimeHandler` instead of `datetime.strptime`

    * handle `Z` in datetime correctly

    * set SameSite property to `Strict`

## 1.3

* use ISO 8601 dateformat

* add `from` GET parameter to `/api/v1/timespans` route (works like `filter_datetime` without JSON body)

* bugfix:

    * use timezone UTC instead of no timezone

    * serialize to `None` and not `"None"`

## 1.2

* add users and authentication

* add user to timespan and setting model

* bugfix: 

    * deserialize also when created_at/updated_at are None

    * deserialize also on different date format

    * check username in request with session

    * allow different date format on `filter_datetime`

## 1.1

* add datetime as possible filter to GET timespans/settings

* add server datetime to response to add this server datetime to the next client request

* only update timespans and settings which are updated

* use [peewee](http://docs.peewee-orm.com) to access database

* bugfix: check start_time and stop_time for `None` and not only `"None"`

## 1.0

* server to collect [*timeslime*](https://gitlab.com/lookslikematrix/timeslime) data

* build docker container

* add route to store settings

* add route for getting all timespans