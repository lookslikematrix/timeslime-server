FROM python:3.10-slim-buster

ENV USER user
ENV PASSWORD supersupersecret

ENV TEST_USER test_user
ENV TEST_PASSWORD supersecret

WORKDIR /timeslime-server
COPY . /timeslime-server
RUN pip install pipenv && pipenv install

EXPOSE 8000

CMD ["./startup.sh"]